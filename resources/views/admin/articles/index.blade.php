@extends('admin.layouts.default')
@section('title', 'Articles')
@section('description','Articles')
@section('css')
@endsection
@section('js')
@endsection
@section('caption')
  @isset($create_record_buttons)
  @foreach($create_record_buttons as $create_record_button)
  @isset($create_record_button['relation_record_id'])
  <a href="/admin/{{$create_record_button['name']}}/new/{{$create_record_button['relation_record_id']}}">
    @else
    <a href="/admin/{{$create_record_button['name']}}/new">
      @endisset
      <button class="btn btn-success ">
        新規{{$create_record_button['name_kana']}}を作成
      </button>
    </a>
    @endforeach
    <hr>
    @endisset

  <h2>{{$model_info['name_kana']}}一覧</h2>
@endsection
@section('content')
<div class="table-responsive">
  <table class="table table-dark text-nowrap">
    <thead>
      <tr>
        <th scope="col">タイトル</th>
        <th scope="col">キャスト名</th>
        <th scope="col">状態</th>
        <th scope="col">店舗</th>
        <th scope="col">日時</th>
        <th scope="col">詳細</th>
      </tr>
    </thead>
    <tbody>
      @foreach($records as $record)
      <tr>
        <th scope="col"><a href="/blog/{{$record->id}}">{{$record->title}}</a></th>
        <th scope="col">
          @if(isset($record->cast)&&isset($record->cast->name))
          {{$record->cast->name}}
          @endif
        </th>
        <th scope="col">{{$status_index[$record->status]}}</th>
        <th scope="col">{{config('const.'.$record->app_name)['shop_name_kana']}}</th>
        <th scope="col">{{$record->created_at->format('Y-m-d')}}</th>
        <th scope="col"><a href="/admin/{{$model_info['name']}}/edit/{{$record->id}}">
            <div class="btn btn-info">詳細</div>
          </a></th>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
{{ $records->links('vendor/pagination/bootstrap-4') }}
@endsection
