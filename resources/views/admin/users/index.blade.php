@extends('admin.layouts.default')
@section('title', 'Users')
@section('description','Users')
@section('css')
@endsection
@section('js')
@endsection
@section('caption')
<h2>{{$model_info['name_kana']}}一覧
  @isset($is_create_record_url)
  <a href="/admin/{{$model_info['name']}}/new">
    <button class="btn btn-success float-right">
      新規{{$model_info['name_kana']}}を作成
    </button>
  </a>
  @endisset
</h2>
@endsection
@section('content')
<div class="table-responsive">
  <table class="table table-dark text-nowrap">
    <thead>
      <tr>
        <th scope="col">id</th>
        <th scope="col">アカウント名</th>
        <th scope="col">メールアドレス</th>
        <th scope="col">アカウント権限種類</th>
        <th scope="col">状態</th>
        <th scope="col">ショップ名</th>
        <th scope="col">詳細</th>

      </tr>
    </thead>
    <tbody>
      @foreach($records as $record)
      <tr>
        <th scope="col">{{$record->id}}</th>
        <th scope="col">{{$record->name}}</th>
        <th scope="col">{{$record->email}}</th>
        <th scope="col">{{['owner'=>'オーナー','staff'=>'スタッフ','cast'=>'cast'][$record->role]}}</th>
        <th scope="col">{{$status_index[$record->status]}}</th>
        <th scope="col">{{config('const.'.$record->app_name)['shop_full_name']}}</th>
        <th scope="col"><a href="/admin/{{$model_info['name']}}/edit/{{$record->id}}">
            <div class="btn btn-info">詳細</div>
          </a></th>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
{{ $records->links('vendor/pagination/bootstrap-4') }}
@endsection
