@extends('admin.layouts.default')
@section('title', 'Articles')
@section('description','Articles')
@section('css')
@endsection
@section('js')
@endsection
@section('caption')
<h2>{{$model_info['name_kana']}}一覧
  @isset($is_create_record_url)
  <a href="/admin/{{$model_info['name']}}/new">
    <button class="btn btn-success float-right">
      新規{{$model_info['name_kana']}}を作成
    </button>
  </a>
  @endisset
</h2>
@endsection
@section('content')
<div class="table-responsive">
  <table class="table table-dark text-nowrap">
    <thead>
      <tr>
        <th scope="col">キャスト</th>
        <th scope="col">出勤or非出勤</th>
        <th scope="col">店舗</th>
        <th scope="col">状態</th>
        <th scope="col">詳細</th>
      </tr>
    </thead>
    <tbody>
      @foreach($records as $record)
      <tr>
        <th scope="col"><a href="/cast/{{$record->id}}">{{$record->name}}</a></th>
        @if((string)$record->is_attendance_in === 'enable')
        <th scope="col">
        <button class="btn btn-success">{{'出勤'}}</button>
        <button class="btn"><a href="/admin/casts/update_attendance/{{$record->id}}/disable">{{'非出勤'}}</a></button>
        </th>
        @else
        <th scope="col">
        <button class="btn"><a href="/admin/casts/update_attendance/{{$record->id}}/enable">{{'出勤'}}</a></button>
        <button class="btn btn-success">{{'非出勤'}}</button>
        </th>
        @endif
        <th scope="col">{{config('const.'.$record->user->app_name)['shop_name']}}</th>
        <th scope="col">{{$status_index[$record->user->status]}}</th>
        <th scope="col"><a href="/admin/{{$model_info['name']}}/edit/{{$record->id}}">
            <div class="btn btn-info">詳細</div>
          </a></th>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
{{ $records->links('vendor/pagination/bootstrap-4') }}
@endsection
