@extends('admin.template.edit')



@section('anothor_form_top')

@if(isset($picture_info['profile']->id))
<img src="{{config('filesystems')['disks'][$picture_info['profile']->app_name]['url']}}/thumbnail/{{$picture_info['profile']->file_name}}"
      width="100">
<hr>
<form method="POST" action="{{ route('admin_pictures_delete', ['id' => $picture_info['profile']->id]) }}">
      {{ csrf_field() }}
      <input class="form-control btn-danger" type="submit" value="プロフィール画像を削除" onclick='return confirm("よろしいですか？");'>
</form>
<hr>
@endif
@if(isset($picture_info['header']->id))
<img src="{{config('filesystems')['disks'][$picture_info['header']->app_name]['url']}}/original/{{$picture_info['header']->file_name}}"
      width="100">
<hr>
<form method="POST" action="{{ route('admin_pictures_delete', ['id' => $picture_info['header']->id]) }}">
      {{ csrf_field() }}
      <input class="form-control btn-danger" type="submit" value="ヘッダー画像を削除" onclick='return confirm("よろしいですか？");'>
</form>
<hr>
@endif
@endsection

@section('additional_form_item_top')
@if(!isset($picture_info['profile']->id))
<div class="form-group">
      <label>プロフィール画像</label>
      <input id="picture" type="file" class="form-control" name="picture_profile" autofocus>
</div>
@endif
@if(!isset($picture_info['header']->id))
<div class="form-group">
      <label>ヘッダー画像</label>
      <input id="picture" type="file" class="form-control" name="picture_header" autofocus>
</div>
@endif
<div class="form-group">
      <label>Eメール</label>
      <input class="form-control" type="email" name="email" value="{{ old('email') ? old('email') : $record->user->email}}">
</div>
<div class="form-group">
      <label>パスワード</label>
      <input class="form-control" type="password" name="password">
</div>
<div class="form-group">
      <label>状態</label>
      <select class="form-control" name="status">
            @foreach(['enable' => '公開','disable' => '非公開'] as $status_key => $status_val)
            @if((string)$status_key === (string)$record->user->status)
            <option value="{{$status_key}}" selected>{{$status_val}}</option>
            @else
            <option value="{{$status_key}}">{{$status_val}}</option>
            @endif
            @endforeach
      </select>
</div>
 <div class="form-group">
      <label>ショップ</label>
      <select class="form-control" name="app_name">
            @foreach($column_infos['app_name']['input_values'] as $app_name_key => $app_name_val)
            @if((string)$app_name_key === (string)$record->user->app_name)
            <option value="{{$app_name_key}}" selected>{{$app_name_val}}</option>
            @else
            <option value="{{$app_name_key}}">{{$app_name_val}}</option>
            @endif
            @endforeach
      </select>
</div> 
@can('staff-higher')
<div class="form-group">
      <label>月間アクセスカウント</label>
      <input class="form-control" type="number" name="monthly_access_count" value="{{ old('monthly_access_count') ? old('tmonthly_access_count') : $record->monthly_access_count}}">
</div>
@endcan
@endsection

@section('additional_form_item_bottom')
<hr>
<div class="form-group">
      <h3>未登録のプロフィール</h3>
      @foreach ($interviews as $interview)
      <label>
            {{$interview->question}}
      </label>
      <textarea class="form-control" name="interviews[{{$interview->id}}]"></textarea>
      @endforeach
</div>
<hr>
<div class="form-group">
      @if($record->interviews->count()>0)
      <h3>登録済みのプロフィール</h3>
      @endisset
      @foreach ($record->interviews as $interview)
      <label>{{$interview->question}}</label>
      <textarea class="form-control" name="interviews[{{$interview->id}}]">{{ $interview->pivot->answer }}</textarea>
      @endforeach
</div>
@endsection

<!-- <div class="form-group">
<label>{$model_info['name_kana']}プロフィール用画像のアップロード(会員に表示されます)</label>
<input id="picture" type="file" class="form-control" name="picture" autofocus>
</div>
endsection

section('image_list')
if(isset($record) && isset($record->pictures) &&$record->pictures->count()>0)
<h3>キャストレコードに紐づく画像の一覧と操作</h3>

foreach($record->pictures()->where('processing_type','original')->get() as $picture)
      <img src="/storage/{$picture->dir_path_name.$picture->file_name}" width="50" height="50">
      <form method="POST" action="{ route('admin_picture_groups_delete', ['id' => $picture->picture_group->id]) }">
      { csrf_field() }
      <label>{$model_info['name_kana']}プロフィール用画像に設定されている上記画像の削除</label>
      <input class="form-control" type="submit" value="削除" onclick='return confirm("よろしいですか？");'>
      </form>
endforeach
endif
-->
