@extends('admin.template.new')

@section('additional_form_item_top')
<div class="form-group">
    <label>画像のアップロード</label>
    <input id="picture" type="file" class="form-control" name="picture" autofocus>
</div>
@endsection
