@extends('admin.template.edit')

@section('anothor_form_top')
@if(isset($picture_info) && isset($picture_info['main']))
<img src="{{config('filesystems')['disks'][$picture_info['main']->app_name]['url']}}/original/{{$picture_info['main']->file_name}}"
    width="50" height="50">
<form method="POST" action="{{ route('admin_pictures_delete', ['id' => $record->picture->id]) }}">
    {{ csrf_field() }}
    <input class="btn btn-s btn-danger" type="submit" value="削除" onclick='return confirm("よろしいですか？");'>
</form>
@endif
@endsection

@section('additional_form_item_top')

@if(isset($record) && !isset($record->picture->id))
<div class="form-group">
    <label>画像のアップロード</label>
    <input id="picture" type="file" class="form-control" name="picture" autofocus>
</div>
@endif
@if(isset($record->cast)&&isset($record->cast->name))
<p>キャスト名: {{$record->cast->name}}</p>
@endif
@endsection
