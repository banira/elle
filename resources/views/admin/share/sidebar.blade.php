
<div class="card">
<nav class="d-md-sidebar" style="background-color: f2f2f2;">
          <div class="sidebar-sticky">
          <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">

            </h6>
            <ul class="nav flex-column">
            <li class="nav-item">
              @can('staff-higher')
              <a class="nav-link active" href="/admin">
                Top
              @endcan

              @can('cast-only')
              <a class="nav-link active" href="/admin/casts/edit/{{Auth::user()->cast->id}}">
                プロフィールの編集
              @endcan
              </a>

              @can('owner-only')
              <li class="nav-item">
                <a class="nav-link" href="/admin/users/owner/index">
                  オーナー
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/admin/users/staff/index">
                  スタッフ
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/admin/categories/index">
                  ブログカテゴリー
                </a>
              </li>
              @endcan

              @can('staff-higher')
              <li class="nav-item">
                <a class="nav-link" href="/admin/casts/index">
                  キャスト
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/admin/interviews/index">
                  プロフィール項目
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/admin/posts/index">
                  お知らせ
                </a>
              </li>
              @endcan


              @can('cast-higher')
              <li class="nav-item">
                <a class="nav-link" href="/admin/articles/index">
                  ブログ記事
                </a>
              </li>
              @endcan

            </ul>


          </div>
        </nav>
</div>
