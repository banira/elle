<header style="margin-bottom: 10px;">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  @can('staff-higher')
  <a class="navbar-brand" href="/admin">{{config('const.'.config('const.app_name'))['shop_name']}}</a>
  @endcan
  @can('cast-only')
  <a class="navbar-brand" href="/admin/articles/index">{{$cast->name}}</a>
  @endcan

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  @auth

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li>
        <form method="POST" action="/admin/logout">
          {{ csrf_field() }}
          <input class="form-control btn btn-danger" value="ログアウト" type="submit">
        </form>
      </li>
    </ul>
  </div>
  @endauth

  </nav>
</header>
