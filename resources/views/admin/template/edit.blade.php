@extends('admin.layouts.default')
@section('title', 'Articles')
@section('description','Articles')
@section('css')
@endsection
@section('js')
@endsection

@section('caption')
  @isset($create_record_buttons)
    @foreach($create_record_buttons as $create_record_button)
      @isset($create_record_button['relation_record_id'])

        <a class="btn btn-success" href="/admin/{{$create_record_button['name']}}/new/{{$create_record_button['relation_record_id']}}">
        @else
        <a class="btn btn-success" href="/admin/{{$create_record_button['name']}}/new">
        @endisset
          新規{{$create_record_button['name_kana']}}を作成
        </a>
        <hr >
    @endforeach
  @endisset

@endsection



      @section('content')
      @yield('anothor_form_top')
      <form accept-charset="UTF-8" method="POST" action="{{ route('admin_'.$model_info['name'].'_update', ['id' => $record->id]) }}"
            enctype="multipart/form-data" files="true">
            {{ csrf_field() }}
            @yield('additional_form_item_top')

            @foreach($model_info['column_names'] as $column_name)
            <div class="form-group mt-3">
                  @if(isset($model_info['hide_columns'])&&array_search($column_name,$model_info['hide_columns'])!==false)
                  {{-- 入力しない情報用 --}}
                  @elseif(isset($model_info['unalterable_columns']) &&
                  array_search($column_name,$model_info['unalterable_columns'])!==false)
                  <label>{{isset($column_infos[$column_name]['name_kana']) ? $column_infos[$column_name]['name_kana'] :
                        $column_name}}</label>
                  <div class="form-group">
                        <p>{{ $record->$column_name }}</p>
                  </div>
                  {{-- select要素用処理 --}}
                  @elseif(isset($column_infos[$column_name]['input_type']) &&
                  $column_infos[$column_name]['input_type']==='select')
                  <label>{{isset($column_infos[$column_name]['name_kana']) ? $column_infos[$column_name]['name_kana'] :
                        $column_name}}</label>
                  <select class="form-control" name="{{$column_name}}">
                        @foreach($column_infos[$column_name]['input_values'] as $option_key => $option_value)
                        @if((string)$option_key === (string)$record->$column_name)
                        <option value="{{$option_key}}" selected>{{$option_value}}</option>
                        @else
                        <option value="{{$option_key}}">{{$option_value}}</option>
                        @endif
                        @endforeach
                  </select>
                  @elseif(isset($column_infos[$column_name]['input_type']) &&
                  $column_infos[$column_name]['input_type']==='textarea')
                  <label>{{isset($column_infos[$column_name]['name_kana']) ? $column_infos[$column_name]['name_kana'] :
                        $column_name}}</label>
                  <textarea class="form-control" name="{{$column_name}}">{{$record->$column_name}}</textarea>
                  {{-- 入力しない+表示しない情報用 --}}
                  @else
                  <label>{{isset($column_infos[$column_name]['name_kana']) ? $column_infos[$column_name]['name_kana'] :
                        $column_name}}</label>

                  <input class="form-control" type="{{isset($column_infos[$column_name]['input_type']) ? $column_infos[$column_name]['input_type'] : 'text'}}"
                        name="{{$column_name}}" value="{{ old($column_name) ? old($column_name) : $record->$column_name }}">
                  @endif
                  @endforeach
                  @yield('additional_form_item_bottom')

                  <hr>
                  <input class="form-control btn-success" type="submit" value="編集する">
      </form>
      <hr>
      @isset($is_delete_record_form)

      <form method="POST" action="{{ route('admin_'.$model_info['name'].'_delete', ['id' => $record->id]) }}">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{$record->id}}">
            <input class="form-control btn-danger" type="submit" value="{{$model_info['name_kana']}}を削除" onclick='return confirm("よろしいですか？");'>
      </form>

      @endisset
      @endsection
