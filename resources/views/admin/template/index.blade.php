@extends('admin.layouts.default')
@section('title', 'Articles')
@section('description','Articles')
@section('css')
@endsection
@section('js')
@endsection
@section('caption')
<h2>{{$model_info['name_kana']}}一覧
  @isset($create_record_buttons)
  @foreach($create_record_buttons as $create_record_button)
  @isset($create_record_button['relation_record_id'])
  <a href="/admin/{{$create_record_button['name']}}/new/{{$create_record_button['relation_record_id']}}">
    @else
    <a href="/admin/{{$create_record_button['name']}}/new">
      @endisset
      <button class="btn btn-success float-right mr-1">
        {{$create_record_button['name_kana']}}を作成
      </button>
    </a>
    @endforeach
    @endisset
</h2>
@endsection
@section('content')
<div class="table-responsive">
  <table class="table table-dark text-nowrap">
    <thead>
      <tr>
        @foreach($column_infos as $column_name => $column_info)
        @if(array_search($column_name,$model_info['hide_columns'])===false)
        <th scope="col text-nowrap">{{isset($column_info['name_kana'])?$column_info['name_kana']:$column_name}}</th>
        @endif
        @endforeach
        <th scope="col">日時</th>
        <th scope="col"></th>

      </tr>
    </thead>
    <tbody>
      @foreach($records as $record)
      <tr>
        @foreach($column_infos as $column_name => $column_info)
        @if(array_search($column_name,$model_info['hide_columns'])===false)
        <th scope="col">{{$record->$column_name}}</th>
        @endif
        @endforeach
        <th scope="col">{{ date_format($record->created_at, 'Y-m-d') }}</th>
        <th scope="col"><a href="/admin/{{$model_info['name']}}/edit/{{$record->id}}">
            <div class="btn btn-info">詳細</div>
          </a></th>

      </tr>
      @endforeach
    </tbody>
  </table>
</div>
{{ $records->links('vendor/pagination/bootstrap-4') }}
@endsection
