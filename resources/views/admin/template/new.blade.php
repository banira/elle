@extends('admin.layouts.default')
@section('title', 'Articles')
@section('description','Articles')
@section('css')
@endsection
@section('js')
@endsection
@section('caption')
@endsection
@section('content')
@isset($relation_record_id)
<form accept-charset="UTF-8" method="POST" action="{{ route('admin_'.$model_info['name'].'_create',['id'=>$relation_record_id]) }}" enctype="multipart/form-data" files="true">
@else
<form accept-charset="UTF-8" method="POST" action="{{ route('admin_'.$model_info['name'].'_create') }}" enctype="multipart/form-data" files="true">
@endisset
{{ csrf_field() }}
@yield('additional_form_item_top')
@foreach($model_info['column_names'] as $column_name)
<div class="form-group">
{{-- 入力しない情報用 --}}
@if(isset($model_info['hide_columns']) && array_search($column_name,$model_info['hide_columns'])!==false)
{{-- select要素用処理 --}}
@elseif(isset($column_infos[$column_name]['input_type'])&&$column_infos[$column_name]['input_type']==='select')
<label>{{isset($column_infos[$column_name]['name_kana']) ? $column_infos[$column_name]['name_kana'] : $column_name}}</label>
<select class="form-control" name="{{$column_name}}">
@foreach($column_infos[$column_name]['input_values'] as $option_key => $option_value)
<option value="{{$option_key}}">{{$option_value}}</option>
@endforeach
</select>
@elseif(isset($column_infos[$column_name]['input_type'])&&$column_infos[$column_name]['input_type']==='textarea')
<label>{{isset($column_infos[$column_name]['name_kana']) ? $column_infos[$column_name]['name_kana'] : $column_name}}</label>
<br><textarea class="form-control" name="{{$column_name}}">{{old($column_name)}}</textarea>
@else
<label>{{isset($column_infos[$column_name]['name_kana']) ? $column_infos[$column_name]['name_kana'] : $column_name}}</label>
{{-- それ以外のinput type用(text email password number等) --}}
      <input class="form-control" type="{{isset($column_infos[$column_name]['input_type']) ? $column_infos[$column_name]['input_type'] : 'text'}}" name="{{$column_name}}" value="{{old($column_name)}}">
@endif

</div>
@endforeach
@yield('additional_form_item_bottom')

<hr>
      <input class="form-control" type="submit">
</form>
@endsection
