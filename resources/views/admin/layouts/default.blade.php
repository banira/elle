<!DOCTYPE html>
<html>
  <head>
    <title>{{config('shop_info.name')}}管理画面</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Hello, world!</title>
    <link rel="stylesheet" href="/css/adm.css">
    @yield('css')
    @if (!app()->environment('production'))
    <meta name="robots" content="noindex, nofollow, noarchive" />
    @endif
<style>
.fixed{
  position: fixed;
  bottom: 0;
  right: 0;
  padding: 14px;
  width: 100%;
   background :rgba(0,0,0,0.3);
  text-align: center;
  z-index: 100;
 }


 .flash_message{
  padding: 20px;
  text-align: center;
  width: 100%;
  background-color: lightYellow;
  margin-bottom: 30px;

 }


 body{
   background-color: #ccc;
 }

 nav{
  margin-bottom: 30px;
 }


 footer{
   padding: 30px 0;
   margin-top: 30px;
   background-color: #fff;
 }

 h2{
   font-size: 20px;
   margin-top: 20px;
   margin-bottom: 20px;
   border-left: #666 8px solid;
   padding: 10px;
   background-color: #999;
 }

 h3{
   font-size: 18px;
   border-left: #666 5px solid;
   padding: 10px;
 }

 h4{
   font-size: 14px;
 }

 textarea{
   margin-bottom: 20px;
   height: 100px;
 }


 tbody th,thead th {
   font-size: 14px;
   font-weight: normal;
 }


 .card{
   margin-bottom: 20px;
   padding: 10px;
 }



 .container {

    padding-right: 5px;
    padding-left: 5px;
    padding-bottom: 20px;
}

.container img{
  width: 200px;
  display: block;
  margin: 0 auto 20px auto;
}
</style>
  </head>
  <body>
    @include('admin/share/header')
    @if (session('flash_message'))
    <div class="flash_message" onclick="this.classList.add('hidden')">{{ session('flash_message') }}</div>
    @endif


  <div class="row">
    <div class="col-lg-3 col-md-3">
      @include('admin/share/sidebar')
    </div>
    <div class="col-lg-9 col-md-9">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
      @yield('caption')
      @yield('content')
      </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    @yield('js')
  </body>
</html>
