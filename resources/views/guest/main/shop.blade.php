@extends('guest.layouts.default')
@section('title','店舗情報｜')
@section('h1','店舗情報｜')
@section('description','店舗情報｜')
@section('content')
<!-- ▼ fv -->
<div class="fv fv-shop">
	<h2 class="page-title">お店のご紹介</h2>
</div>
<!-- ▲ fv -->
<main class="cont-shop">
	<!-- ▼ Shop Concept -->
	<section>
	  <h3 class="title"><span>Shop Concept</span></h3>
	  <p>{{$app_info['shop_concept']}}</p>
	</section>
	<!-- ▲ Shop Concept -->
	<!-- ▼ Shop Information -->
	<section>
	  <h3 class="title"><span>Shop Information</span></h3>
	  <table>
		<tr>
		  <th width="95px">店名</th>
		  <td>{{$app_info['shop_name']}}（{{$app_info['shop_name_kana']}}）岡山店</td>
		</tr>
		<tr>
		  <th>業種</th>
		  <td>{{$app_info['genre_name_kana']}}</td>
		</tr>
		  <tr>
		  <th>所在地</th>
		  <td>{{$app_info['shop_full_address']}}</td>
		</tr>
		<tr>
		  <th>営業時間</th>
		  <td>{{$app_info['shop_time']}}</td>
		</tr>
		<tr>
		  <th>電話番号</th>
		  <td>TEL. <a href="tel:{{$app_info['shop_tel']}}"> {{$app_info['shop_tel']}}</a></td>
		</tr>
		<tr>
		  <th>座席</th>
		  <td>{{$app_info['chair']}}</td>
		</tr>
		<tr>
		  <th>キャスト数</th>
		  <td>{{$app_info['num_cast']}}名</td>
		</tr>
		<tr>
		  <th>カラオケ</th>
		  <td>有り</td>
		</tr>
	  </table>
	</section>
	<!-- ▲ Shop Information -->
	<!-- ▼ System -->
	<section>
	  <h3 class="title"><span>System</span></h3>
	  <table>
		<tr>
		  <th width="95px">カウンター</th>
		  <td>1set 50min 4,000円</td>
		</tr>
		<tr>
		  <th>ボックス</th>
		  <td>1set 50min 4,000円</td>
		</tr>
		  <tr>
		  <th>延長料金</th>
		  <td>50min 4000円</td>
		</tr>
		<tr>
		  <th>VIProom</th>
		  <td>なし</td>
		</tr>
		<tr>
		  <th>飲み放題</th>
		  <td>有り</td>
		</tr>
		<tr>
		  <th>飲み放題料金</th>
		  <td>1set 50min 5,000円</td>
		</tr>
		<tr>
		  <th>指名料</th>
		  <td>1,000円</td>
		</tr>
		<tr>
		  <th>キープ料</th>
		  <td>4,000円</td>
		</tr>
		<tr>
		  <th>クレジットカード</th>
		  <td>使用可</td>
		</tr>
		<tr>
		  <th>予算</th>
		  <td>5,000円～<br>消費税8% サービス15%<br>5000円(税・サ込み)</td>
		</tr>
		<tr>
		  <th>時間帯料金</th>
		  <td> 20時10分までに来られたお客様は<br>1時間 2500円(税・サ込み)<br>そのまま延長でセット1000円引き</td>
		</tr>
	  </table>
	</section>
	<!-- ▲ System -->
	<!-- ▼ Access map -->
	<div id="access" class="link link-access"></div>
	<section>
	  <h3 class="title"><span>Access map</span></h3>
	  <iframe src="https://www.google.com/maps/embed?pb={{$app_info['shop_googlemap_address']}}" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
	</section>
	<!-- ▲ Access map -->
	<!-- ▼ TOP btn -->
	<div class="btn btn-top">
		<a class="btn" href="/"><i class="fas fa-home"></i>TOPへ戻る</a>
	</div>
	<!-- ▲ TOP btn -->
</main>
@endsection