@extends( 'guest.layouts.default' )
@section('title','')
@section('h1','')
@section('description','')
@section( 'css' )
<!-- css -->
<link rel="stylesheet" type="text/css" href="/css/top.css"/>
@endsection
@section( 'content' )
<body class="atom top" id="wrapper">
	<div class="wrapper">
		<div class="slider">
			<!-- ▼ slider -->
			<div class="slick-box">
				<figure>
					<a href="/cast"><div class="slick-img lazyload" data-bg="/img/top/fv-cast.jpg"></div></a>
				</figure>
				<figure>
					<a href="/blog"><div class="slick-img lazyload" data-bg="/img/top/fv-diary.jpg"></div></a>
				</figure>
				<figure>
					<a href="/recruit"><div class="slick-img lazyload" data-bg="/img/top/fv-recruit.jpg"></div></a>
				</figure>
			</div>
			<!-- ▲ slider -->
			<!-- ▼ concept -->
			<div class="concept-bg">
				<div class="concept">
					<p>{{$app_info['shop_concept']}}</p>
				</div>
			</div>
			<!-- ▲ concept -->
		</div>
		<main>
			<!-- ▼ club ATOM’s Cast -->
			<section class="cast">
				<div class="link" id="link-cast"></div>
				<h2 class="title-top"><small>キャストのご紹介</small>{{$app_info['shop_name']}}’s Cast</h2>
				<div class="link" id="link-cast"></div>
				@include('guest.share.casts',['is_morelink'=>true,'name_ja'=>'キャストのご紹介','name_en'=>'club ATOM’s Cast'])
			</section>
			<!-- ▲ club ATOM’s Cast -->
			<section class="diary">
				<div class="link" id="link-diary"></div>
				<h2 class="title-top"><small>キャスト日記</small>Cast Diary</h2> @include('guest.share.articles',['is_morelink'=>true])
			</section>
			<!-- ▼ Cast Ranking -->
			<section class="ranking">
				<div class="link" id="link-ranking"></div>
				<h2 class="title-top"><small>キャストランキング</small>Cast Ranking</h2>
				@include('guest.share.casts',['is_morelink'=>false,'casts'=>$ranking_casts,'name_ja'=>'キャストランキング','name_en'=>'Cast Ranking'])
			</section>
			<!-- ▼ News & Topics -->
			<section class="news">
				<div class="link" id="link-news"></div>
				<h2 class="title-top"><small>{{$app_info['shop_name']}}からのお知らせ</small>News &amp; Topics</h2>
				@include('guest.share.posts',['is_morelink'=>true])
			</section>
			<!-- ▲ News & Topics -->
			<!-- ▼ Recruit -->
			<section class="top-recruit">
				<div class="link" id="link-recruit"></div>
				<a href="/recruit"><img class="lazyload" src="/img/top/banner.jpg" alt="{{$app_info['banner']}}"></a> </section>
			<!-- ▲ Recruit -->
		</main>
	</div>
</body>
@endsection
