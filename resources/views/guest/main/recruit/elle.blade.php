@extends('guest.layouts.default')
@section('title','求人情報｜')
@section('h1','求人情報｜')
@section('description','求人情報｜')
@section('content')
<!-- ▼ fv -->
<div class="fv fv-recruit">
<h2 class="page-title">求人情報</h2>
</div>
<!-- ▲ fv -->
<main class="cont-recruit">
<!-- ▼ Message -->
<section>
  <h3 class="title"><span>Message</span></h3>
  <p>【男性社員急募！！】働きやすさに自信あり！！<br>
	  当店は他店と比べ 時給以外に稼げる内容が盛り沢山♪<br>
	  ドリンクバックとは別にボトル、来客、同伴に対しても報酬が支払われます♪<br>
	  女の子に稼いで貰える報酬システムになっております♪</p>
 <!-- ▼ 1項目 -->
 <h4 class="subtitle">最低保証時給以外にもたくさんのバックを用意しています</h4>
 <p>土曜日は時給+500円、つまり最低保証3000円！（レギュラーは時給3200円！）<br>その他同伴バック2000円、指名バック(1000円)、ドリンクバック(200円〜1000円)、ボトルバック（20%）、皆勤手当、やる気次第でさらに稼げます。</p>
 <!-- ▲ 1項目 -->
 <!-- ▼ 1項目 -->
 <h4 class="subtitle">ノルマ・営業・アフターの強制はありません</h4>
 <p>時給高いし、バックの種類も豊富で本当にノルマないの？と疑問に思うかもしれませんが、本当にありません。お店自体に集客は任せてのびのび働いて下さい。</p>
 <!-- ▲ 1項目 -->
 <!-- ▼ 1項目 -->
 <h4 class="subtitle">提携託児所あり。保育料負担致します。</h4>
 <p>シングルマザーや働くママには嬉しい託児所のサポートシステムがあります。<br>保育料の半額負担(1ヶ月上限20000円)します！<br>今働いているキャストにもこのシステムを利用している子が何名もいますよ^^</p>
 <!-- ▲ 1項目 -->
 <!-- ▼ 1項目 -->
 <h4 class="subtitle">お酒飲めなくてOK</h4>
 <p>ノンアルコール系のドリンクを豊富に用意しているので、飲めなくも安心して働いてもらえます。<br>実際に飲めない子も半分以上いますし、車で通勤も問題ありません。</p>
 <!-- ▲ 1項目 -->
 <!-- ▼ 1項目 -->
 <h4 class="subtitle">営業時間はきっちり1時で終わります</h4>
 <p>あまり遅くまで働きたくない、早上がりできると言われてたのに気がつけばズルズルと遅い時間まで....<br>そんな話はよく聞きますが、atomは法令遵守で営業しております。<br>1時にはお店がおわるので、安心して働いてください。</p>
 <!-- ▲ 1項目 -->
 <!-- ▼ 1項目 -->
 <h4 class="subtitle">18〜30歳の女の子が楽しく働いています</h4>
 <p>メインの年齢層は22〜27くらいで女の子の年齢層は比較的若めです。<br>3ヶ月に1回程度みんなで食事会したり、花見のような季節ごとのイベントも自由参加で開催してます。</p>
 <!-- ▲ 1項目 -->
 <!-- ▼ 1項目 -->
 <h4 class="subtitle">シフトは自主申告制です</h4>
 <p>シフトは出勤できる日を申告してもらっています。<br>「週末は必ず出勤して」とかは言いません。<br>申告の頻度も特に月に1回とか週に1回とか決めていないのであなたのペースでOKです。</p>
 <!-- ▲ 1項目 -->
 <!-- ▼ 1項目 -->
 <h4 class="subtitle">最後に</h4>
 <p>創業5年でまだまだ業界では若手ではありますが、ニューオープンのお店のように、せわしないようなこともなく、安心して働いてもらえるような制度やサポート体制はしっかり整っています。<br>これからも女の子が稼げて、働きやすい状態をキープしながら発展していくのが目標です。<br>求人情報を読んで気になったかたは質問だけでもいいので連絡して下さい！！</p>
 <!-- ▲ 1項目 -->
</section>
<!-- ▲ Message -->
<!-- ▼ Recruit Information -->
<section>
  <h3 class="title"><span>Recruit Information</span></h3>
  <!-- ▼ 募集要項 -->
  <h4 class="subtitle">募集要項</h4>
  <table>
	<tr>
	  <th width="80px">業種</th>
	  <td>ラウンジ</td>
	</tr>
	<tr>
	  <th>職種</th>
	  <td>1) ホステス レギュラー<br>
		  2) ホステス アルバイト<br>
		  3) ウェイター 社員（男女共に募集）週休二日<br>
		  4) ウェイター アルバイト（男女共に募集）<br>
		  5) ドライバー</td>
	</tr>
	<tr>
	  <th>給与</th>
	  <td>時給3000円〜5000円<br><br>
		  指名バック：1000円〜<br>
		  ドリンクバック：200円〜1000円<br>
		  同伴バック：2000円<br>
		  場内指名：300円<br>
		  皆勤賞：出勤日ごとに1000円<br>
		  ボトルバック：ボトル料金の20%(1000円〜1万円)<br><br>
		  3) 月給25万円〜60万円 週休2日可能 経験者優遇<br>
		  4) 時給1000円〜（金曜日時給1100円〜 土曜日時給1200円〜）<br>
		  5) 日給5000円〜</td>
	</tr>
	<tr>
	  <th>勤務日</th>
	  <td>1) 月曜日～土曜日の中で週5日以上<br>
		  2) 月曜日～土曜日の中で応相談（週1回からでも可）<br>
		  3) 週休二日可能<br>
		  4) 月曜日～土曜日の中で応相談（週1回からでも可）</td>
	</tr>
	<tr>
	  <th>勤務時間</th>
	  <td>1.2) 20時〜25時<br>
		  3.4) 17時〜25時</td>
	</tr>
	<tr>
	  <th>従業員情報</th>
	  <td>男性3名　女性20人</td>
	</tr>
	<tr>
	  <th>応募資格</th>
	  <td>18歳以上</td>
	</tr>
	<tr>
	  <th>待遇</th>
	  <td><ul>
		  	<li>送迎あり</li>
		  	<li>衣装貸与</li>
		  	<li>日払可</li>
		  	<li>未経験者教育制度</li>
		  	<li>1日体験入店可</li>
		  	<li>寮有り(即入居OK)</li>
		  	<li>提携託児所あり 上限20000円<br>他</li>
		  </ul></td>
	</tr>
	<tr>
	  <th>休日</th>
	  <td>日曜</td>
	</tr>
	<tr>
	  <th>勤務地</th>
	  <td>{{$app_info['shop_full_address']}}</td>
	</tr>
  </table>
  <!-- ▲ 募集要項 -->
</section>
<!-- ▲ Recruit Information -->
<!-- ▼ 応募btn -->
<section>
  <h3 class="title">ご応募はこちら</h3>
  <p class="btn-text">お気軽にお問い合わせください。</p>
  <div class="btns">
	<a class="btn btn-mail" href="mailto:{{$app_info['mail']}}"><i class="fa fa-envelope" aria-hidden="true"></i> メールでのお問い合わせ</a>
	<a class="btn btn-tel" href="tel:{{$app_info['tel']}}"><i class="fas fa-phone-square" aria-hidden="true"></i> 電話でのお問い合わせ</a>
	<a class="btn btn-line" href="{{$app_info['line']}}" target="_blank"><!--<i class="fab fa-line"></i>--><img src="/img/recruit/icon_line.png" altt="line@"> LINE@でのお問い合わせ</a>
  </div>
</section>
<!-- ▲ 応募btn -->
<!-- ▼ TOP btn -->
<div class="btn btn-top">
	<a class="btn" href="/"><i class="fas fa-home"></i>TOPへ戻る</a>
</div>
<!-- ▲ TOP btn -->
</main>
@endsection
