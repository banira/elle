<!doctype html>
<html lang="ja">
	@include('guest.share.head')
	<body class="atom top" id="wrapper">
		<h1 class="description">@yield('h1')岡山の{{$app_info['genre_name_kana']}} {{$app_info['shop_name']}}（{{$app_info['shop_name_kana']}}）@yield('h1_after')</h1>
		@include('guest.share.header')
		@yield('content')
		@include('guest.share.footer')
		@include('guest.share.js')
	</body>
</html>