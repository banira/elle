<ul>
	@foreach($posts as $post)
	<!-- ▼ 1件 -->
	<li><a href="{{route('guest_post_view',['id'=>$post->id])}}"><small>{{$post->created_at->format('Y-m-d')}}</small>{{$post->body}}</a>
	</li>
	<!-- ▲ 1件 -->
	@endforeach
	<!-- ▼ btn -->
	@if($is_morelink === true)
	<div class="btn"> <a href="/news"><i class="fas fa-newspaper"></i>お知らせをもっと読む</a> </div>
	@endif
	<!-- ▲ btn -->
</ul>