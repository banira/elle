<header id="header">
	<!-- ▼ header logo -->
	<a href="/"><img class="logo" src="/img/logo.png" alt="{{$app_info['shop_full_name']}}"></a>
	<!-- ▲ header logo -->
	<!-- ▼ header btn -->
	<ul class="header-btn">
		<li class="header-btn-line"><a href="{{$app_info['lineat']}}" target="_blank"><img src="/img/lineat.png" alt="line@">お問い合わせ</a>
		</li>
	</ul>
	<!-- ▲ header btn -->
	<!-- ▼ toggle -->
	<div id="toggle">
		<div>
			<span></span>
			<span></span>
			<span></span>
			<div class="menu">menu</div>
		</div>
	</div>
	<!-- ▲ toggle -->
	<!-- ▼ nav -->
	<nav id="nav">
		<ul>
			<li><a href="/">TOP</a>
			</li>
			<li><a href="/shop">お店のご紹介</a>
			</li>
			<li><a href="/cast">キャストのご紹介</a>
			</li>
			<li><a href="{{route('guest_article_index')}}">キャスト日記</a>
			</li>
			<li><a href="/shop#access">アクセス</a>
			</li>
			<li><a href="/recruit">求人情報</a>
			</li>
		</ul>
	</nav>
	<!-- ▲ nav -->
</header>