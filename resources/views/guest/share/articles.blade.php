<div class="diary-list">
	@foreach($articles as $article)
	<!-- ▼ 1件 -->
	<a href="{{route('guest_article_view',['cast_id'=>$article->cast->id,'id'=>$article->id])}}">
		<div class="diary-list">
			<div class="diary-list-text">
				@if(isset($article->cast)&& $article->cast->pictures()->count()>0)
				<div class="diary-cast-img lazyload" data-bg="/thumbnail/{{$article->cast->pictures->first()->file_name}}">
				@else
				<div class="diary-cast-img lazyload" data-bg="/img/{{$app_name}}/noimage.png">
					@endif
                    @if($article->cast && $article->cast->is_attendance_in === 'enable')
					<span><img class="lazyload" src="/img/icon-attendance-min.svg" alt="出勤中"></span>
					@endif
				</div>
				<span class="diary-cast">{{$article->cast->name}}の日記</span>
				<p><small>{{$article->created_at->format('Y-m-d')}}</small>{{$article->title}}</p>
				</div>
				@if(isset($article->picture))
					<div class="diary-list-img lazyload" data-bg="/thumbnail/{{$article->picture->file_name}}"></div>
					@else
					<div class="diary-list-img lazyload" data-bg="/img/{{$app_name}}/noimage.png"></div>
				@endif
		</div>
	</a>
	<!-- ▲ 1件 -->
	@endforeach
</div>
<!-- ▼ btn -->
@if($is_morelink === true)
<div class="btn"><a class="btn" href="{{route('guest_article_index')}}"><i class="fas fa-book-open"></i>日記をもっと読む</a></div>
@endif
<!-- ▲ btn -->
