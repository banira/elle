<!-- ▼ club ATOM’s Cast -->
<ul class="cast-list">
	@foreach($casts as $cast)
	<!-- ▼ 1名 -->
	<li> <a href="{{route('guest_cast_view',['id'=>$cast->id])}}">
            @if($cast->pictures()->where('type','profile')->count()>0)
            <div class="cast-img lazyload" data-bg="/thumbnail/{{$cast->pictures()->where('type','profile')->first()->file_name}}">
            @else
            <div class="cast-img lazyload" data-bg="/img/{{$app_name}}/noimage.png">
            @endif
            @if($cast->is_attendance_in === 'enable')
            <span>
                <img class="lazyload" src="/img/icon-attendance.svg" alt="出勤中">
            </span>
            @endif
            </div>
              <span class="cast-name">{{$cast->name}}</span>
            </a>

	</li>
	<!-- ▲ 1名 -->
	@endforeach @if($is_morelink === true)
	<!-- ▼ 1名 -->
	<li class="more">
		<a href="{{route('guest_cast_index')}}">
			<div class="cast-img lazyload" data-bg="/img/{{$app_name}}/cast-noimg.png">
				<div class="cast-more-bg">
					<div class="cast-more"><i class="fas fa-search-plus"></i>キャストを<br>もっと見る
					</div>
				</div>
				<div class="cast-more-border"></div>
			</div>
		</a>
	</li>
	<!-- ▲ 1名 -->
	@endif
</ul>
