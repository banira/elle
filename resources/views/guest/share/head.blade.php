<head>
	<!-- meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="format-detection" content="email=no,telephone=no,address=no">
	<meta property="og:image" content="https://club-atom.site/img/top.jpg"/> 
	<meta name="keywords" content="岡山,{{$app_info['genre_name_kana']}},{{$app_info['shop_name_kana']}},{{$app_info['shop_name']}}">
	<meta name="description" content="@yield('description')岡山の{{$app_info['genre_name_kana']}} {{$app_info['shop_name']}}（{{$app_info['shop_name_kana']}}）の公式サイトです。{{$app_info['shop_comment']}}岡山で{{$app_info['genre_name_kana']}}をお探しの方は是非一度 {{$app_info['shop_full_name']}}にお越し下さい。">
	<!--[if lt IE 9]>
	<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- title -->
	<title>@yield('title')岡山の{{$app_info['genre_name_kana']}} {{$app_info['shop_name']}}（{{$app_info['shop_name_kana']}}）@yield('title_after')</title>

	<!-- favicon -->
	<link rel="icon" type="x-icon" href="/img/favicon.ico">
	<link rel="apple-touch-icon" sizes="180x180" href="/img/apple-touch-icon-180x180.png">
	<link rel="shortcut icon" href="/img/favicon.ico"> <!-- IE対応用 -->

	<!-- reset -->
	<link rel="stylesheet" type="text/css" href="/css/ress.min.css"/>

	<!-- css -->
	<link rel="stylesheet" type="text/css" href="/css/common.css"/>
	@yield('css')

	<!-- font -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

	<!-- slick.css -->
	<link rel="stylesheet" type="text/css" href="/css/slick.css"/>
	<link rel="stylesheet" type="text/css" href="/css/slick-theme.css"/>

	<!-- lightbox -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.7.1/css/lightbox.css" rel="stylesheet">
    @include('guest.share.analytics.'.config('const.app_name'))
</head>