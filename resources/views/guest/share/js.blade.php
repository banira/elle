<!-- jquery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- cookie -->
<script src="/js/jquery.cookie.js"></script>

<!-- nav -->
<script>
	$( function () {
		var $win = $( window ),
			$main = $( '#wrapper' ),
			$nav = $( '#header' ),
			navHeight = $nav.outerHeight(),
			navPos = $nav.offset().top,
			fixedClass = 'is-fixed';

		$win.on( 'load scroll', function () {
			var value = $( this ).scrollTop();
			if ( value > navPos ) {
				$nav.addClass( fixedClass );
				$main.css( 'margin-top', navHeight );
			} else {
				$nav.removeClass( fixedClass );
				$main.css( 'margin-top', '0' );
			}
		} );
	} );
	( function ( $ ) {
		$( function () {
			var $header = $( "#header" );
			// Nav Fixed
			$( window ).scroll( function () {
				if ( $( window ).scrollTop() > 350 ) {
					$header.addClass( "fixed" );
				} else {
					$header.removeClass( "fixed" );
				}
			} );
			// Nav Toggle Button Open
			$( "#toggle" ).click( function () {
				$header.toggleClass( "open" );
			} );
			// Nav Toggle Button hide
			$( "#nav a" ).click( function () {
				$header.removeClass( "open" );
			} );
		} );
	} )( jQuery );
</script>

<!-- smooth scroll -->
<script>
	$( 'a[href^="#"]' ).click( function () {
		// スクロールの速度
		var speed = 400; // ミリ秒で記述
		var href = $( this ).attr( "href" );
		var target = $( href == "#" || href == "" ? 'html' : href );
		var position = target.offset().top;
		$( 'body,html' ).animate( {
			scrollTop: position
		}, speed, 'swing' );
		return false;
	} );
</script>

<!-- Lazy Load -->
<script src="/js/lazysizes.min.js"></script>
<script src="/js/ls.unveilhooks.min.js"></script>
<script>
	// 背景画像の遅延読み込み（lazy load）をするlazysizes.js
	document.addEventListener( 'lazybeforeunveil', function ( e ) {
		var bg = e.target.getAttribute( 'data-bg' );
		if ( bg ) {
			e.target.style.backgroundImage = 'url(' + bg + ')';
		}
	} );
</script>

<!-- slick.js -->
<script src="/js/slick.min.js"></script>
<script>
	/* slider_fv */
	$( function () {
		$( '.slick-box' ).slick( {
			centerMode: true, //センターモード
			centerPadding: '40px', //前後のパディング
			autoplay: true, //オートプレイ
			autoplaySpeed: 2000, //オートプレイの切り替わり時間
			arrows: false, // 前後の矢印非表示
			slidesToShow: 1,
			dots: true
		} );
	} );
</script>

<!-- lightbox -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.7.1/js/lightbox.min.js" type="text/javascript"></script>
<script>
	$( document ).on( 'click', '[data-toggle="lightbox"]', function ( event ) {
		event.preventDefault();
		$( this ).ekkoLightbox();
	} );
</script>
