<!-- ▼ footer -->
<div class="information">
	<div class="information-title">
		<div class="shop-img lazyload" data-bg="/img/shop/shop-fv.jpg"></div>
		{{$app_info['shop_name']}}’s Information
	</div>
	<img class="lazyload" src="/img/logo-s.png" alt="{{$app_info['shop_full_name']}}">
	<strong>{{$app_info['shop_full_name']}}</strong><br>{{$app_info['shop_full_address']}}<br> TEL. <a href="tel:{{$app_info['shop_tel']}}">{{$app_info['shop_tel']}}</a><br>営業時間：{{$app_info['shop_time']}}
	<ul class="sns">
		<li><a href="{{$app_info['sns_tw']}}" target="_blank"><i class="fab fa-twitter"></i></a>
		</li>
		<li><a href="{{$app_info['sns_fb']}}" target="_blank"><i class="fab fa-facebook-f"></i></a>
		</li>
		<li><a href="{{$app_info['sns_insta']}}" target="_blank"><i class="fab fa-instagram"></i></a>
		</li>
	</ul>
	<!-- ▼ btn -->
	<div class="btn">
		<a href="/shop"><i class="fas fa-building"></i>店舗情報をもっと見る</a>
	</div>
	<!-- ▲ btn -->
</div>
<!-- ▲ footer-->
<!-- ▼ footer -->
<footer>
	<ul>
		<li><a href="mailto:">お問い合わせ</a>
		</li>
		<li><a href="privacy">プライバシーポリシーについて</a>
		</li>
	</ul>
	<div class="copyright">© {{$app_info['shop_name']}}.</div>
</footer>
<!-- ▲ footer-->
<!-- ▼ float btn -->
<div class="float-btn">
	<ul>
		<li class="footer-btn-line"><a href="{{$app_info['lineat']}}" target="_blank"><img src="/img/lineat.png" alt="line@">クーポン</a>
		</li>
		<li class="footer-btn-phone"><a href="tel:{{$app_info['shop_tel']}}"><i class="fas fa-phone-square"></i>お問い合わせ</a>
		</li>
		<li class="footer-btn-top"><a href="#wrapper"><i class="fas fa-angle-up"></i></a>
		</li>
	</ul>
</div>
<!-- ▲ float btn-->