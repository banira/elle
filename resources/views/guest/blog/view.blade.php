@extends('guest.layouts.default')
@section('title',$article->title.'｜')
@section('title_after',$article->cast->name.'のキャスト日記')
@section('h1',$article->title.'｜')
@section('h1_after',$article->cast->name.'のキャスト日記')
@section('description',$article->title.'｜岡山の'.$app_info['genre_name_kana'].' '.$app_info['shop_name'].'（'.$app_info['shop_name_kana'].'）'.''.$article->cast->name.'のキャスト日記｜')
@section('content')
<!-- ▼ fv -->
@if(isset($article->cast)&& $article->cast->pictures()->where('type','header')->count()>0)
<div class="fv fv-cast" style="background-image: url(/thumbnail/{{$article->cast->pictures()->where('type','header')->first()->file_name}})">

@else
<div class="fv fv-cast" style="background-image: url(/img/{{$app_name}}/noimage.png)">

@endif
	<h2 class="page-title page-title-cast">
			@if(isset($article->cast)&& $article->cast->pictures()->where('type','profile')->count()>0)
<div class="page-title-cast-img lazyload" data-bg="/thumbnail/{{$article->cast->pictures()->where('type','profile')->first()->file_name}}"></div>
@else
<div class="page-title-cast-img lazyload" data-bg="/img/{{$app_name}}/noimage.png"></div>
@endif
		  {{$article->cast->name}}<small>’s blog</small></h2>
</div>
<!-- ▲ fv -->
<main class="page-diary">
	<h3 class="title">{{$article->title}}</h3>
	@if(isset($article)&& $article->picture)
	<img class="eyecatch lazyload" src="/thumbnail/{{$article->picture->file_name}}" alt="">
		@else
		<img class="eyecatch lazyload" src="/img/{{$app_name}}/noimage.png" alt="">
		@endif
	<p>{{$article->body}}</p>
	<!-- ▼ postlink -->
	<ul class="postlink">
	@isset($article_prev)
		<li><a href="{{route('guest_article_view',['cast_id'=>$article_prev->cast->id,'article_id'=>$article_prev->id])}}">前の記事へ</a></li>
	@endisset
	@isset($article_next)
	<li><a href="{{route('guest_article_view',['cast_id'=>$article_next->cast->id,'article_id'=>$article_next->id])}}">次の記事へ</a></li>
	@endisset
	</ul>
	<!-- ▲ postlink -->
	<!-- ▼ btn -->
	<div class="btn">
		<a class="btn" href="{{route('guest_article_index')}}"><i class="fas fa-list"></i>一覧に戻る</a>
	</div>
	<!-- ▲ btn -->
	<!-- ▼ この記事を書いたキャスト-->
	<h3 class="diary-cast-title">この記事を書いたキャスト</h3>
	<div class="diary-cast">
		  @if(isset($article->cast)&& $article->cast->pictures->where('type','profile')->count()>0)
		<div class="diary-cast-img lazyload" data-bg="/thumbnail/{{$article->cast->pictures->where('type','profile')->first()->file_name}}">
		@else
		<div class="diary-cast-img lazyload" data-bg="/img/{{$app_name}}/noimage.png">
		@endif
			  <div class="diary-cast-border"></div>
			  <div class="diary-cast-frame"></div>
			  @if($article->cast->is_attendance_in === 'enable')
			  <span><img class="lazyload" src="/img/icon-attendance.svg" alt="出勤中"></span>
			  @endif
		  </div>
		<h4 class="diary-cast-name">{{$article->cast->name}}</h4>
		<p>{{$article->cast->message}}</p>
		<!-- ▼ btn -->
		<div class="btn">
			<a class="btn" href="/cast/{{$article->cast->id}}"><i class="fas fa-user-circle"></i>プロフィールを見る</a>
		</div>
		<!-- ▲ btn -->
	</div>
	<!-- ▲ この記事を書いたキャスト -->
	<!-- ▼ 一覧 -->
	<section class="page-list diary">
		<h4 class="subtitle page-list">よく読まれている記事</h4>
		  <div class="diary-list">
			<!-- ▼ 1件 -->
			@foreach($new_articles as $new_article)
			<a href="{{route('guest_article_view',['cast_id'=>$new_article->cast->id,'id'=>$new_article->id])}}">
			  <div class="diary-list">
				<div class="diary-list-text">
				  @if(isset($new_article->cast)&& $new_article->cast->pictures->where('type','profile')->count()>0)
					<div class="diary-cast-img lazyload" data-bg="/thumbnail/{{$new_article->cast->pictures->first()->file_name}}">
					@else
					<div class="diary-cast-img lazyload" data-bg="/img/{{$app_name}}/noimage.png">
					@endif
			@if($new_article->cast->is_attendance_in === 'enable')
			  <span><img class="lazyload" src="/img/icon-attendance-min.svg" alt="出勤中"></span>
			  @endif
				  </div>
				  <span class="diary-cast">{{$new_article->cast->name}}の日記</span>
				  <p><small>{{$new_article->created_at->format('Y-m-d')}}</small>{{$new_article->title}}</p>
				</div>
				<div class="diary-list-img lazyload" data-bg="/img/cast/kirara/cover.jpg"></div>
			  </div>
			</a>
			@endforeach
			<!-- ▲ 1件 -->
		  </div>
		</section>
	<!-- ▲ 一覧 -->
	<!-- ▼ Category -->
	<section class="page-list">
	@if($categories->count()>0)
		<h4 class="subtitle"><span>Category</span></h4>
		<ul>
		@foreach($categories as $category)
		@if($category->articles_count>0)
			<li><a href="{{route('guest_article_index_cast_category',['cast_id'=>$article->cast->id,'slug'=>$category->slug])}}">{{$category->name}}({{$category->articles_count}})</a></li>
			@endif
		@endforeach
		</ul>
		@endif
	</section>
	<!-- ▲ Category -->
	<!-- ▼ Archive -->
	@if(isset($archive_infos))
	<section class="page-list">
		<h4 class="subtitle"><span>Archive</span></h4>
		<ul>
		@forelse($archive_infos as $archive_info)
			<li><a href="{{route('guest_article_index_cast_date',['cast_id'=>$cast_id,'year'=>preg_replace('/年\d+月/','',$archive_info->created_at),'month'=>preg_replace('/月/','',preg_replace('/\d+年/','',$archive_info->created_at))])}}">{{$archive_info->created_at}} ({{$archive_info->count}})</a></li>
        @empty
		@endforelse
		</ul>
	</section>
	@endif
	<!-- ▲ Archive -->
	<!-- ▼ TOP btn -->
	<div class="btn btn-top">
		<a class="btn" href="/"><i class="fas fa-home"></i>TOPへ戻る</a>
	</div>
	<!-- ▲ TOP btn -->
</main>
@endsection
