@extends('guest.layouts.default')

@section('title')
@if(isset($cast->name)){{$page_title.'の記事一覧｜'}}@else
{{'キャスト日記 '.$page_title.'｜'}}@endif
@endsection
@section('title_after')
@if(isset($cast->name)){{$cast->name.'のキャスト日記'}}@else
{{''}}@endif
@endsection
@section('h1')
@if(isset($cast->name)){{$page_title.'の記事一覧｜'}}@else
{{'キャスト日記 '.$page_title.'｜'}}@endif
@endsection
@section('h1_after')
@if(isset($cast->name)){{$cast->name.'のキャスト日記'}}@else
{{''}}@endif
@endsection
@section('description')
@if(isset($cast->name))
{{$page_title.'｜岡山の'.$app_info['genre_name_kana'].' '.$app_info['shop_name'].'（'.$app_info['shop_name_kana'].'）'.''.$cast->name.'のキャスト日記｜'}}@else
{{'キャスト日記 '.$page_title.'｜'}}@endif
@endsection
@section('content')
<main class="cont-diary">
<!-- ▼ 一覧 -->
<!-- ▼ Cast Diary -->
<section class="diary">
	<div class="link" id="link-diary"></div>
	<h2 class="title-top"><small>キャスト日記</small>Cast Diary</h2> @include('guest.share.articles',['is_morelink'=>false])
</section>
<!-- ▲ 一覧 -->
<!-- ▼ pagination -->
{{ $articles->links('vendor.pagination.default') }}
<!-- ▲ pagination -->
<!-- ▼ TOP btn -->
<div class="btn btn-top">
	<a class="btn" href="/"><i class="fas fa-home"></i>TOPへ戻る</a>
</div>
<!-- ▲ TOP btn -->
</main>
@endsection