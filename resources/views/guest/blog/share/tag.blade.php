<section class="box_blog_list blog">
    <div class="cat-list">
        <ul>
            @foreach($tags as $tag)
            @if($tag->articles_count>0)
            <li>
                <a href="/blog/tag/{{$tag->slug}}">{{$tag->name}}<small>({{$tag->articles_count}})</small></a>
            </li>
            @endif
            @endforeach
        </ul>
    </div>
</section>