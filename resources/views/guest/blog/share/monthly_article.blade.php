<ul>
    @foreach($archive_infos as $archive_info)
    <li><a href="{{route('guest_blog_index_date',['year'=>preg_replace('/年\d+月/','',$archive_info->created_at),'month'=>preg_replace('/月/','',preg_replace('/\d+年/','',$archive_info->created_at))])}}">{{$archive_info->created_at}}<small>({{$archive_info->count}})</small></a></li>
    @endforeach
</ul>