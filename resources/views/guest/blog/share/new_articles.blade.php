<h3>最近の投稿</h3>
<ul>
    @foreach($new_articles as $new_article)
    <li>
        <a href="/blog/{{$new_article->category->slug}}/{{$new_article->slug}}">{{$new_article->title}}</a>
        <span class="post-date">{{$new_article->created_at->format('Y-m-d')}}</span>
    </li>
    @endforeach
</ul>