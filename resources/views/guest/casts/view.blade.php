@extends('guest.layouts.default')
@section('title',$cast->name.'｜')
@section('h1',$cast->name.'｜')
@section('description',$cast->name.'のプロフィール｜')
@section('content')
<!-- ▼ fv -->
@if(isset($picture_info['header']->file_name))
<div class="fv" style="background-image: url(/thumbnail/{{$picture_info['header']->file_name}})">
@else
<div class="fv" style="background-image: url(/img/{{$app_name}}/noimage.png)">
@endif
	<h2 class="page-title">キャストのご紹介</h2>
</div>
<!-- ▲ fv -->
<main class="page-cast">
	<h3 class="title-top"><small>Cast</small>{{$cast->name}}</h3>
	@if($cast->pictures()->where('type','profile')->count()>0)
	<div class="page-cast-img lazyload" data-bg="/thumbnail/{{$picture_info['profile']->file_name}}">
@else
<div class="page-cast-img lazyload" data-bg="/img/{{$app_name}}/noimage.png">
@endif
		@if($cast->is_attendance_in === 'enable')
		<span>
			<img class="lazyload" src="/img/icon-attendance-min.svg" alt="出勤中">
		</span>
		@endif
	</div>
	<p>{{$cast->message}}</p>
	<!-- ▼ イメージ写真 -->
	@if($cast->pictures()->count()>0)
	<ul class="cast-img-lightbox">
	@foreach($cast->pictures as $picture)
		<a href="/public/thumbnail/{{$picture->file_name}}" data-lightbox="lightbox"><li class="lazyload" data-bg="/storage/thumbnail/{{$picture->file_name}}"></li></a>
	@endforeach
	</ul>
	@endif
	<!-- ▲ イメージ写真 -->
	<!-- ▼ Cast Profile -->
	<section>
		<h4 class="subtitle"><span>Cast Profile</span></h4>
		<table>
		@foreach($cast->interviews as $interview)
			<tr>
				<th width="80px">{{$interview->question}}</th>
				<td>{{$interview->pivot->answer}}</td>
			</tr>
		@endforeach
		</table>
	</section>
	<!-- ▲ Cast Profile -->
	<!-- ▼ btn -->
	<div class="btn">
		<a class="btn" href="{{route('guest_cast_index')}}"><i class="fas fa-list"></i>キャスト一覧に戻る</a>
	</div>
	<!-- ▲ btn -->
	<!-- ▼ 一覧 -->
	@if($articles->count()>0)
	<section class="page-list diary">
		<h5 class="subtitle page-list">{{$cast->name}}の日記</h5>
		@include('guest.share.articles',['is_morelink'=>false])
		<!-- ▼ btn -->
		<div class="btn">
			<a class="btn" href="{{route('guest_article_index_cast',['cast_id'=>$cast->id])}}"><i class="fas fa-book-open"></i>日記をもっと見る</a>
		</div>
		<!-- ▲ btn -->
	</section>
	<!-- ▲ 一覧 -->
	@endif
	<!-- ▼ TOP btn -->
	<div class="btn btn-top">
		<a class="btn" href="/"><i class="fas fa-home"></i>TOPへ戻る</a>
	</div>
	<!-- ▲ TOP btn -->
</main>
@endsection