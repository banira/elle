@extends('guest.layouts.default')
@section('title','キャスト一覧｜')
@section('h1','キャスト一覧｜')
@section('description','キャスト一覧｜')
@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.7.1/css/lightbox.css" rel="stylesheet">
<link href="/css/sweetalert.css" rel="stylesheet">
@endsection
@section('content')
<main class="cont-news">
	<!-- ▼ club ATOM’s Cast -->
	<div class="link" id="link-cast"></div>
	<h2 class="title-top"><small>キャスト一覧</small>club {{$app_info['shop_name']}}'s Cast</h2>
	<!-- ▼ 一覧 -->
	@include('guest.share.casts',['is_morelink'=>false,'name_ja'=>'キャスト一覧','name_en'=>'club ATOM’s Cast'])
	<!-- ▲ 一覧 -->
	<!-- ▼ pagination -->
	{{$casts->links()}}
	<!-- ▲ pagination -->
	<!-- ▼ TOP btn -->
	<div class="btn btn-top">
		<a class="btn" href="/"><i class="fas fa-home"></i>TOPへ戻る</a>
	</div>
	<!-- ▲ TOP btn -->
</main>
@endsection