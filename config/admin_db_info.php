<?php
return [
    'validation' => [
        'articles' => [
            'title' => ['required', 'max:255'],
            'body' => ['required'],
            'app_name' => ['required', 'in:' . env('ALL_APP_NAMES', '')],
            'status' => ['required'],
            'category' => ['required'],
        ],
        'posts' => [
            'body' => ['required'],
            'app_name' => ['required', 'in:' . env('ALL_APP_NAMES', '')],
            'status' => ['required', 'in:enable,disable,ban'],
        ],
        'users' => [
            'name' => ['required', 'max:255'],
            'email' => ['required', 'email'],
            'role' => ['required', 'in:owner,staff,cast'],
            'app_name' => ['required'],
            'password' => ['alpha_num', 'min:6', 'max:50'],
            'status' => ['required', 'in:enable,disable,ban'],

        ],
        'casts' => [
            'name' => ['required', 'max:255'],
            'password' => ['alpha_num', 'min:6', 'max:50'],
            'status' => ['required', 'in:enable,disable,ban'],
            'message' => [
            ],
            'age' => [
                'max:255',
            ],
            'is_attendance_in' => [
                'max:255',
            ],
            'picture_profile' => [
                'image',
                // アップロードされたファイルであること
                'file',
                // 最小縦横120px 最大縦横400px
                //'dimensions:min_width=120,min_height=120,max_width=400,max_height=400',
            ],
            'picture_header' => [
                'image',
                // アップロードされたファイルであること
                'file',
                // 最小縦横120px 最大縦横400px
                //'dimensions:min_width=120,min_height=120,max_width=400,max_height=400',
            ],

        ],
        'interviews' => [
            'question' => ['required', 'unique:interviews'],
        ],
        'categories' => [
            'name' => ['required', 'unique:categories'],
            'slug' => ['required', 'unique:categories'],
        ],
    ],

    'column_infos' => [
        'articles' => [
            'title' => [
                'name_kana' => 'タイトル',
            ],
            'body' => [
                'name_kana' => '本文',
                'input_type' => 'textarea',
            ],
            'status' => [
                'name_kana' => '状態',
                'input_type' => 'select',
                'input_values' => [
                    'enable' => '公開',
                    'disable' => '非公開',
                ],
            ],
            'app_name' => [
                'name_kana' => 'ショップ',
                'input_type' => 'select',
                'input_values' => array_combine(explode(',', env('ALL_APP_NAMES', null)), explode(',', env('ALL_APP_NAMES', null))),
            ],
            'updated_at' => [
                'name_kana' => '更新日時',
            ],
            'created_at' => [
                'name_kana' => '作成日時',
            ],

        ],
        'users' => [
            'updated_at' => [
                'name_kana' => '更新日時',
            ],
            'created_at' => [
                'name_kana' => '作成日時',
            ],

            'name' => [
                'name_kana' => 'アカウント名',
            ],
            'email' => [
                'name_kana' => 'メールアドレス',
                'input_type' => 'email',
            ],
            'password' => [
                'name_kana' => 'パスワード',
                'input_type' => 'password',
            ],
            'role' => [
                'name_kana' => 'アカウント権限種類',
                'input_type' => 'select',
                'input_values' => [
                    'owner' => 'オーナー',
                    'staff' => 'スタッフ',
                    'cast' => 'キャスト',
                ],
            ],
            'status' => [
                'name_kana' => '状態',
                'input_type' => 'select',
                'input_values' => [
                    'enable' => '公開',
                    'disable' => '非公開',
                    'ban' => '凍結',
                ],
            ],
            'app_name' => [
                'name_kana' => 'ショップ',
                'input_type' => 'select',
                'input_values' => array_combine(explode(',', env('ALL_APP_NAMES', null)), explode(',', env('ALL_APP_NAMES', null))),
            ],
        ],
        'casts' => [
            'updated_at' => [
                'name_kana' => '更新日時',
            ],
            'created_at' => [
                'name_kana' => '作成日時',
            ],

            'name' => [
                'name_kana' => 'キャスト名',
            ],
            'email' => [
                'name_kana' => 'メールアドレス',
            ],
            'age' => [
                'name_kana' => '年齢',
            ],
            'password' => [
                'name_kana' => 'パスワード',
                'input_type' => 'password',
            ],
            'message' => [
                'name_kana' => 'メッセージ',
            ],
            'status' => [
                'name_kana' => '状態',
                'input_type' => 'select',
                'input_values' => [
                    'enable' => '公開',
                    'disable' => '非公開',
                    'ban' => '凍結',
                ],
            ],
            'app_name' => [
                'name_kana' => 'ショップ',
                'input_type' => 'select',
                'input_values' => array_combine(explode(',', env('ALL_APP_NAMES', null)), explode(',', env('ALL_APP_NAMES', null))),
            ],
            'is_attendance_in' => [
                'input_type' => 'select',
                'name_kana' => '出勤or退勤',
                'input_values' => [
                    'enable' => '出勤中',
                    'disable' => '退勤中',
                ],
            ],

        ],

        'categories' => [
            'updated_at' => [
                'name_kana' => '更新日時',
            ],
            'created_at' => [
                'name_kana' => '作成日時',
            ],

            'name' => [
                'name_kana' => 'カテゴリー名',
            ],
            'slug' => [
                'name_kana' => 'スラッグ',
            ],
        ],
        'interviews' => [
            'updated_at' => [
                'name_kana' => '更新日時',
            ],
            'created_at' => [
                'name_kana' => '作成日時',
            ],

            'question' => [
                'name_kana' => 'プロフィール項目',
            ],
        ],
        'posts' => [
            'updated_at' => [
                'name_kana' => '更新日時',
            ],
            'created_at' => [
                'name_kana' => '作成日時',
            ],
            'body' => [
                'name_kana' => '本文',
                'input_type' => 'textarea',
            ],
            'app_name' => [
                'name_kana' => 'ショップ',
                'input_type' => 'select',
                'input_values' => array_combine(explode(',', env('ALL_APP_NAMES', null)), explode(',', env('ALL_APP_NAMES', null))),
            ],
            'status' => [
                'name_kana' => '状態',
                'input_type' => 'select',
                'input_values' => [
                    'enable' => '公開',
                    'disable' => '非公開',
                    'ban' => '凍結',
                ],
            ],
        ],
    ],
];
