<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin'], function () {

    // 全ユーザに許可する操作
    Route::group(['middleware' => ['auth', 'can:cast-higher']], function () {
        // ユーザ一覧
        Route::get('/', 'Admin\CastsController@index');
        Route::post('/logout', 'Admin\Auth\LoginController@logout')->name('logout');
        Route::get('/articles/new/{id}', 'Admin\ArticlesController@new');
        Route::get('/articles/index', 'Admin\ArticlesController@index');
        Route::post('/articles/create/{id}', 'Admin\ArticlesController@create')->name('admin_articles_create');
        Route::get('/articles/edit/{id}', 'Admin\ArticlesController@edit');
        Route::post('/articles/update/{id}', 'Admin\ArticlesController@update')->name('admin_articles_update');
        Route::post('/articles/delete/{id}', 'Admin\ArticlesController@delete')->name('admin_articles_delete');
        Route::get('/casts/edit/{id}', 'Admin\CastsController@edit');
        Route::post('/casts/update/{id}', 'Admin\CastsController@update')->name('admin_casts_update');
        Route::post('/pictures/delete/{id}', 'Admin\PicturesController@delete')->name('admin_pictures_delete');

    });

    // staff以上
    Route::group(['middleware' => ['auth', 'can:staff-higher']], function () {

        Route::get('/casts/index', 'Admin\CastsController@index');
        Route::get('/casts/new', 'Admin\CastsController@new');
        Route::get('/casts/update_attendance/{id}/{value}', 'Admin\CastsController@update_attendance')->name('admin_casts_update_attendance');

        Route::post('/casts/create', 'Admin\CastsController@create')->name('admin_casts_create');

        Route::get('/interviews/index', 'Admin\InterviewsController@index');
        Route::get('/interviews/new', 'Admin\InterviewsController@new');
        Route::post('/interviews/create', 'Admin\InterviewsController@create')->name('admin_interviews_create');
        Route::get('/interviews/edit/{id}', 'Admin\InterviewsController@edit');
        Route::post('/interviews/update/{id}', 'Admin\InterviewsController@update')->name('admin_interviews_update');
        Route::post('/interviews/delete/{id}', 'Admin\InterviewsController@delete')->name('admin_interviews_delete');

        Route::get('/posts/index', 'Admin\PostsController@index');
        Route::get('/posts/new', 'Admin\PostsController@new');
        Route::post('/posts/create', 'Admin\PostsController@create')->name('admin_posts_create');
        Route::get('/posts/edit/{id}', 'Admin\PostsController@edit');
        Route::post('/posts/update/{id}', 'Admin\PostsController@update')->name('admin_posts_update');
        Route::post('/posts/delete/{id}', 'Admin\PostsController@delete')->name('admin_posts_delete');

        Route::get('/pictures/new', 'Admin\PicturesController@new');
        Route::post('/pictures/create', 'Admin\PicturesController@create')->name('admin_pictures_create');
        Route::get('/pictures/edit/{id}', 'Admin\PicturesController@edit');
        Route::post('/pictures/update/{id}', 'Admin\PicturesController@update')->name('admin_pictures_update');

    });

    // システム管理者のみ
    Route::group(['middleware' => ['auth', 'can:owner-only']], function () {

        Route::post('/casts/delete/{id}', 'Admin\CastsController@delete')->name('admin_casts_delete');

        //index action pages
        Route::get('/users/{role}/index', 'Admin\UsersController@index')->where('role', 'owner|staff');
        //new action pages
        Route::get('/users/new', 'Admin\UsersController@new');
//create action pages
        Route::post('/users/create', 'Admin\UsersController@create')->name('admin_users_create');
//edit action pages
        Route::get('/users/edit/{id}', 'Admin\UsersController@edit');
//update action pages
        Route::post('/users/update/{id}', 'Admin\UsersController@update')->name('admin_users_update');
//delete action pages
        Route::post('/users/delete/{id}', 'Admin\UsersController@delete')->name('admin_users_delete');

        Route::get('/categories/index', 'Admin\CategoriesController@index');
        Route::get('/categories/new', 'Admin\CategoriesController@new');
        Route::post('/categories/create', 'Admin\CategoriesController@create')->name('admin_categories_create');
        Route::get('/categories/edit/{id}', 'Admin\CategoriesController@edit');
        Route::post('/categories/update/{id}', 'Admin\CategoriesController@update')->name('admin_categories_update');
        Route::post('/categories/delete/{id}', 'Admin\CategoriesController@delete')->name('admin_categories_delete');

    });
});

Route::get('/', 'MainController@top');
Route::get('/cast', 'CastsController@index')->name('guest_cast_index');
Route::get('/blog', 'ArticlesController@index')->name('guest_article_index');
Route::get('/shop', 'MainController@shop');
Route::get('/privacy', 'MainController@privacy');
Route::get('/recruit', 'MainController@recruit');
Route::get('/system', 'MainController@system');
Route::get('/cast/{cast_id}/blog/{article_id}', 'ArticlesController@view')
    ->where('cast_id', '[1-9][0-9]*')
    ->where('article_id', '[1-9][0-9]*')->name('guest_article_view');
Route::get('/cast/{cast_id}/blog', 'ArticlesController@index')
    ->where('cast_id', '[1-9][0-9]*')->name('guest_article_index_cast');
Route::get('/cast/{cast_id}/blog/cat/{slug}', 'ArticlesController@index')
    ->where('cast_id', '[1-9][0-9]*')->name('guest_article_index_cast_category');
Route::get('/cast/{cast_id}/blog/{year}-{date}', 'ArticlesController@index')
    ->where('cast_id', '[1-9][0-9]*')->name('guest_article_index_cast_date');
Route::get('/cast', 'CastsController@index')->name('guest_cast_index');

Route::get('/cast/{id}', 'CastsController@view')->name('guest_cast_view');

Route::get('/8RjREpgEA6jZdIOTnUXtytvaFJUjzg', 'Admin\Auth\LoginController@loginform');
Route::post('/8RjREpgEA6jZdIOTnUXtytvaFJUjzg', 'Admin\Auth\LoginController@login')->name('login');

Route::get('*', function () {
    abort(404);
});
Route::post('*', function () {
    abort(404);
});


