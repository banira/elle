<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCastsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('casts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('age')->default('');
            $table->integer('user_id');
            $table->string('message')->default('');
            $table->string('is_attendance_in')->default('disable');
            $table->integer('daily_access_count')->default(0);
            $table->integer('weekly_access_count')->default(0);
            $table->integer('monthly_access_count')->default(0);
            $table->integer('total_access_count')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('casts');
    }
}
