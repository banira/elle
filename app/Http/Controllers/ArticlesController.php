<?php

namespace App\Http\Controllers;

use App\Article;
use App\Cast;
use App\Category;
use App\Picture;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArticlesController extends Controller
{
    /**
     * 一覧ページ
     * @return \Illuminate\Http\Response
     * @param \Illuminate\Http\Request $request
     */
    public function index(Request $request, $param1 = '', $param2 = '', $param3 = '')
    {
        $app_name = config('const.app_name');
        $articles = Article::whereHas('cast.user', function ($query) {$query->where('status', 'enable');})->where('status', 'enable')
            ->where('app_name', $app_name);
        $page_title = '';

        $new_articles = Article::whereHas('cast.user', function ($query) {$query->where('status', 'enable');})->where('status', 'enable')
            ->where('app_name', $app_name);

        $new_articles = $new_articles->orderBy('created_at', 'desc')->take(6)->get();
        $categories = Category::withCount('articles');

        $archive_infos = DB::table('articles')
            ->select(DB::raw('DATE_FORMAT(created_at, "%Y年%m月") as created_at'), DB::raw('COUNT(*) as count'))
            ->where('status', 'enable');
        $cast_id = 0;

        if ($param1 !== '') {
            $articles = $articles
                ->where('cast_id', $param1);
            $archive_infos = $archive_infos->where('cast_id', $param1)->where('app_name', $app_name);
            $categories = $categories->whereHas('articles', function ($query) use ($param1, $app_name) {
                $query->where('cast_id', $param1)->where('app_name', $app_name)->where('status', 'enable');
            });
            $cast_id = $param1;
        }
        $cast = Cast::where('id', $cast_id)->first();
        $picture_info['profile'] = Picture::where('cast_id', $cast_id)->where('type', 'profile')->first();

        $archive_infos = $archive_infos->groupBy(DB::raw('DATE_FORMAT(created_at, "%Y年%m月")'))
            ->orderBy('created_at', 'desc')
            ->get();
        $categories = $categories->orderBy('articles_count', 'desc')->get();
        if ($request->route()->named('guest_article_index_cast_category')) {
            $cat = Category::where('slug', $param2)->firstOrFail();
            $articles = $articles->whereHas('category', function ($query) use ($param2) {
                $query->where('slug', $param2);
            });
            $page_title .= $cat->name;
        } elseif ($request->route()->named('guest_article_index_cast_date')) {
            $articles = $articles->whereYear('created_at', $param2)->whereMonth('created_at', $param3);
            $page_title .= $param2 . '年' . $param3 . '月';
        }
        $articles = $articles->orderBy('created_at', 'desc')
            ->paginate(6);
        $page_title = $page_title === '' ? '記事一覧' : $page_title;

        return view('guest.blog.index')
            ->with('app_info', config('const.' . $app_name))
            ->with('cast_id', $param1)
            ->with('app_name', $app_name)
            ->with('articles', $articles)
            ->with('new_articles', $new_articles)
            ->with('archive_infos', $archive_infos)
            ->with('categories', $categories)
            ->with('page_title', $page_title)
            ->with('picture_info', $picture_info)
            ->with('cast', $cast);
    }

    /**
     * 詳細ページ
     * @return \Illuminate\Http\Response
     * @param \Illuminate\Http\Request $request
     */
    public function view(Request $request, $cast_id, $article_id)
    {
        $app_name = config('const.app_name');

        $article = Article::where('status', 'enable')
            ->where('cast_id', $cast_id)
            ->where('id', $article_id)
            ->whereHas('cast.user', function ($query) {$query->where('status', 'enable');})
            ->where('app_name', $app_name)
            ->firstOrFail();
        $articles = Article::where('status', 'enable')
            ->whereHas('cast.user', function ($query) {$query->where('status', 'enable');})
            ->where('app_name', $app_name);
        $new_articles = clone $articles;

        $new_articles = $new_articles->orderBy('created_at', 'desc')->take(6)->get();
        $categories = Category::withCount(['articles' => function ($query) use ($cast_id, $app_name) {
            $query->where('cast_id', $cast_id); //->where('app_name', $app_name)->where('status', 'enable');
        }])->orderBy('articles_count', 'desc')->get();
        $archive_infos = DB::table('articles')
            ->select(DB::raw('DATE_FORMAT(created_at, "%Y年%m月") as created_at'), DB::raw('COUNT(*) as count'))
            ->where('status', 'enable')
            ->where('cast_id', $cast_id)
            ->groupBy(DB::raw('DATE_FORMAT(created_at, "%Y年%m月")'))
            ->orderBy('created_at', 'desc')
            ->get();
        $article_prev = Article::where('status', 'enable')->where('app_name', $app_name)->where('id', '<', $article->id)->where('cast_id', $article->cast->id)->orderBy('created_at', 'asc')->first();
        $article_next = Article::where('status', 'enable')->where('app_name', $app_name)->where('id', '>', $article->id)->where('cast_id', $article->cast->id)->orderBy('created_at', 'desc')->first();
        $cast = $article->cast;
        $picture_info['profile'] = Picture::where('cast_id', $cast->id)->where('type', 'profile')->first();
        $picture_info['header'] = Picture::where('cast_id', $cast->id)->where('type', 'header')->first();
        return view('guest.blog.view')
            ->with('app_info', config('const.' . $app_name))
            ->with('app_name', $app_name)
            ->with('article', $article)
            ->with('cast_id', $cast_id)
            ->with('article_prev', $article_prev)
            ->with('article_next', $article_next)
            ->with('picture_info', $picture_info)
            ->with('new_articles', $new_articles)
            ->with('archive_infos', $archive_infos)
            ->with('categories', $categories);
    }
}
