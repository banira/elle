<?php

namespace App\Http\Controllers;

use App\Article;
use App\Cast;
use App\Picture;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CastsController extends Controller
{
    /**
     * 一覧ページ
     * @return \Illuminate\Http\Response
     * @param \Illuminate\Http\Request $request
     */
    public function index(Request $request)
    {
        $app_name = config('const.app_name');
        $casts = Cast::whereHas('user', function ($query) use ($app_name) {
            $query->where('app_name', $app_name)->where('status', 'enable');
        })
            ->orderBy('updated_at', 'desc')
            ->paginate(9);
        return view('guest.casts.index')
            ->with('app_info', config('const.' . $app_name))
            ->with('app_name', $app_name)
            ->with('casts', $casts);
    }

    /**
     * 詳細ページ
     * @return \Illuminate\Http\Response
     * @param \Illuminate\Http\Request $request
     */
    public function view(Request $request, $id)
    {
        $app_name = config('const.app_name');
        $cast = Cast::where('id', $id)
        /*where('status', 'enable')*/
            ->whereHas('user', function ($query) use ($app_name) {
                $query->where('app_name', $app_name)->where('status', 'enable');
            })
            ->firstOrFail();

        $articles = Article::where('status', 'enable')
            ->where('app_name', $app_name)
            ->where('cast_id', $cast->id)
            ->whereHas('cast.user', function ($query) {$query->where('status', 'enable');})
            ->orderBy('updated_at', 'desc')
            ->take(6)
            ->get();

        $access_casts = $request->session()->get('access_casts', null);

        //配列が存在しない場合 配列は存在するが該当のshopidが見つからない場合 配列もshopidもある日付が1日以上立っていた場合
        if ($access_casts === null) {
            $access_casts = $request->session()->put('access_casts', [$id => Carbon::now()->timestamp]);
            $cast = Cast::where('id', $id)->first();
            $cast->total_access_count = $cast->total_access_count + 1;
            $cast->daily_access_count = $cast->daily_access_count + 1;
            $cast->weekly_access_count = $cast->weekly_access_count + 1;
            $cast->monthly_access_count = $cast->monthly_access_count + 1;
            $cast->timestamps = false;
            $cast->save();
        } elseif (!isset($access_casts[$id])) {
            $access_casts[$id] = Carbon::now()->timestamp;
            $access_casts = $request->session()->put('access_casts', $access_casts);
            $cast = Cast::where('id', $id)->first();
            $cast->total_access_count = $cast->total_access_count + 1;
            $cast->daily_access_count = $cast->daily_access_count + 1;
            $cast->weekly_access_count = $cast->weekly_access_count + 1;
            $cast->monthly_access_count = $cast->monthly_access_count + 1;
            $cast->timestamps = false;
            $cast->save();
        } elseif (floor(abs($access_casts[$id] - Carbon::now()->timestamp) / (60 * 60 * 24)) > 0) {
            $access_casts[$id] = Carbon::now()->timestamp;
            $access_casts = $request->session()->put('access_casts', $access_casts);
            $cast = Cast::where('id', $id)->first();
            $cast->total_access_count = $cast->total_access_count + 1;
            $cast->daily_access_count = $cast->daily_access_count + 1;
            $cast->weekly_access_count = $cast->weekly_access_count + 1;
            $cast->monthly_access_count = $cast->monthly_access_count + 1;
            $cast->timestamps = false;
            $cast->save();
        }

        $picture_info['profile'] = Picture::where('cast_id', $cast->id)->where('type', 'profile')->first();
        $picture_info['header'] = Picture::where('cast_id', $cast->id)->where('type', 'header')->first();
        return view('guest.casts.view')
            ->with('app_info', config('const.' . $app_name))
            ->with('app_name', $app_name)
            ->with('picture_info', $picture_info)
            ->with('articles', $articles)
            ->with('cast', $cast);
    }

}
