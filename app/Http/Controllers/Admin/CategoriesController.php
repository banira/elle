<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

//
class CategoriesController extends Controller
{

    public function index(Request $request)
    {
        $params = [];
        $categories = Category::orderBy('created_at', 'desc')->paginate(10);
        $params['records'] = $categories;

        $params['model_info']['name'] = 'categories';
        $params['model_info']['name_kana'] = 'ブログカテゴリー';
        $params['column_infos'] = config('admin_db_info.column_infos')['categories'];
        $params['model_info']['column_names'] = Category::descrive();
        $params['model_info']['hide_columns'] = ['created_at', 'updated_at'];
        $params['create_record_buttons'] = [
            ['name_kana' => 'カテゴリー', 'name' => 'categories'],
        ];

        return view('admin/template/index', $params);
    }

    public function edit(Request $request, $id)
    {
        $category = Category::where('id', $id)->firstOrFail();
        $params['record'] = $category;
        $params['column_infos'] = config('admin_db_info.column_infos')['categories'];
        $params['model_info']['name'] = 'categories';
        $params['model_info']['name_kana'] = 'ブログカテゴリー';
        $params['model_info']['column_names'] = Category::descrive();
        $params['model_info']['hide_columns'] = [];
        $params['model_info']['unalterable_columns'] = ['id', 'member_id', 'message_id', 'created_at', 'updated_at'];
        $params['is_delete_record_form'] = true;
        $params['is_create_record_url'] = true;

        return view('admin/template/edit', $params);
    }

    function new (Request $request) {
        $params['column_infos'] = config('admin_db_info.column_infos')['categories'];

        $params['model_info']['name'] = 'categories';
        $params['model_info']['name_kana'] = 'ブログカテゴリー';
        $params['model_info']['column_names'] = Category::descrive();
        $params['model_info']['hide_columns'] = ['id', 'created_at', 'updated_at'];
        return view('admin/template/new', $params);
    }

    public function delete(Request $request, $id)
    {
        $this->validate($request, [
            'id' => 'required|integer',
        ]);
        $category = Category::where('id', $id)->firstOrFail();
        $category->delete();
        $request->session()->flash('flash_message', 'ブログカテゴリーを削除しました');
        return redirect('admin/categories/index');

    }

    public function update(Request $request, $id)
    {
        $validation = config('admin_db_info.validation')['categories'];
        $validation['name'] = [
            'required',
            Rule::unique('categories')->ignore($id),
        ];
        $validation['slug'] = [
            'required',
            Rule::unique('categories')->ignore($id),
        ];
        $this->validate($request, $validation);

        $category = Category::where('id', $id)->firstOrFail();

        $params['record'] = $category;
        $params['model_info']['name'] = 'categories';
        $params['model_info']['name_kana'] = 'ブログカテゴリー';
        $params['column_infos'] = Category::descrive();

        foreach ($validation as $column_name => $val) {
            if ($request->has($column_name)) {
                var_dump($column_name);
                $category->$column_name = $request->input($column_name);
            }
        }

        if ($category->save()) {
            $request->session()->flash('flash_message', 'ブログカテゴリーを更新しました');
        }

        return redirect('admin/categories/edit/' . $category->id);
    }

    public function create(Request $request)
    {
        $validation = config('admin_db_info.validation')['categories'];

        $this->validate($request, $validation);

        $category = new Category;

        $params['record'] = $category;
        $params['model_info']['name'] = 'categories';
        $params['model_info']['name_kana'] = 'ブログカテゴリー';
        $params['column_infos'] = Category::descrive();

        foreach ($validation as $column_name => $val) {
            if ($request->has($column_name)) {
                $category->$column_name = $request->input($column_name);
            }
        }

        if ($category->save()) {
            $request->session()->flash('flash_message', 'ブログカテゴリーを新規作成しました');
        }

        return redirect('admin/categories/index');
    }
}
