<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Picture;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

//
class PicturesController extends Controller
{

    public function index(Request $request)
    {

        $params = [];
        $pictures = Picture::orderBy('created_at', 'desc')->paginate(10);
        $params['records'] = $pictures;
        $params['model_info']['name'] = 'pictures';
        $params['column_infos'] = config('admin_db_info.column_infos')['pictures'];
        $params['model_info']['name_kana'] = '画像';
        $params['model_info']['column_names'] = Picture::descrive();
        $params['model_info']['hide_columns'] = [];
        return view('admin/template/index', $params);
    }

    public function edit(Request $request, $id)
    {
        $params['column_infos'] = config('admin_db_info.column_infos')['pictures'];
        $picture = Picture::where('id', $id)->firstOrFail();
        $params['record'] = $picture;
        $params['model_info']['name'] = 'pictures';
        $params['model_info']['name_kana'] = '画像';
        $params['model_info']['column_names'] = Picture::descrive();
        $params['model_info']['hide_columns'] = [];
        $params['model_info']['unalterable_columns'] = ['id', 'created_at', 'updated_at'];
        $params['is_delete_record_form'] = true;
        $params['is_create_record_url'] = true;

        return view('admin/template/edit', $params);
    }

    function new (Request $request) {
        $params['column_infos'] = config('admin_db_info.column_infos')['pictures'];
        $params['model_info']['name'] = 'pictures';
        $params['model_info']['name_kana'] = '画像';
        $params['model_info']['column_names'] = Picture::descrive();
        $params['model_info']['hide_columns'] = [];
        return view('admin/template/new', $params);
    }

    public function delete(Request $request, $id)
    {
        $picture = Picture::where('id', $id)->firstOrFail();
        Storage::disk($picture->app_name)->delete('original/' . $picture->file_name);
        Storage::disk($picture->app_name)->delete('thumbnail/' . $picture->file_name);

        $picture->delete();

        $request->session()->flash('flash_message', '画像を削除しました');
        return redirect()->back();
    }

    public function update(Request $request, $id)
    {
        $validation = config('validation.pictures');
        $this->validate($request, $validation);

        $picture = Picture::where('id', $id)->firstOrFail();

        $params['record'] = $picture;
        $params['model_info']['name'] = 'pictures';
        $params['model_info']['name_kana'] = 'チャットマッチング';
        $params['column_infos'] = Picture::descrive();

        foreach ($validation as $column_name => $val) {
            if ($request->has($column_name)) {
                var_dump($column_name);
                $picture->$column_name = $request->input($column_name);
            }
        }

        if ($picture->save()) {
            $request->session()->flash('flash_picture', 'チャットマッチングを更新しました');
        }

        return redirect('admin/pictures/edit/' . $picture->id);
    }

    public function create(Request $request)
    {
        $validation = config('validation.pictures');
        $this->validate($request, $validation);

        $picture = new Picture;

        $params['record'] = $picture;
        $params['model_info']['name'] = 'pictures';
        $params['model_info']['name_kana'] = 'チャットマッチング';
        $params['column_infos'] = Picture::descrive();

        foreach ($validation as $column_name => $val) {
            if ($request->has($column_name)) {
                var_dump($column_name);

                $picture->$column_name = $request->input($column_name);
            }
        }

        if ($picture->save()) {
            $request->session()->flash('flash_picture', 'チャットマッチングを新規作成しました');
        }

        return redirect('admin/pictures/index');
    }
}
