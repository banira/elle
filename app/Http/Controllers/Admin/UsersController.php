<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

//
class UsersController extends Controller
{

    public function index(Request $request, $role)
    {

        $params = [];
        $users = User::where('role', $role)->orderBy('created_at', 'desc')->paginate(10);
        $params['column_infos'] = config('admin_db_info.column_infos')['users'];
        $params['records'] = $users;
        $params['model_info']['column_names'] = User::descrive();
        $params['model_info']['name'] = 'users';
        $params['model_info']['name_kana'] = ['owner' => 'オーナー', 'staff' => 'スタッフ', 'cast' => 'キャスト'][$role];
        $params['model_info']['hide_columns'] = ['password', 'remember_token', ''];
        $params['is_create_record_url'] = true;
        if (Gate::allows('owner-only')) {
            $params['status_index'] = ['enable' => '公開', 'disable' => '非公開', 'ban' => '凍結'];
        } else {
            $params['status_index'] = ['enable' => '公開', 'disable' => '非公開', 'ban' => '非公開'];
        }

        return view('admin/users/index', $params);
    }

    public function edit(Request $request, $id)
    {
        $user = User::where('id', $id)->firstOrFail();
        $params['record'] = $user;
        $params['model_info']['column_names'] = User::descrive();
        $params['model_info']['name'] = 'users';
        $params['model_info']['name_kana'] = 'アカウント';
        $params['column_infos'] = config('admin_db_info.column_infos')['users'];
        $params['model_info']['hide_columns'] = ['password', 'remember_token', ''];
        $params['model_info']['unalterable_columns'] = ['id', 'created_at', 'updated_at'];
        $params['is_delete_record_form'] = true;
        $params['is_create_record_url'] = true;
        $params['column_infos']['role']['input_values'] = [
            'owner' => 'オーナー',
            'staff' => 'スタッフ',
        ];

        return view('admin/users/edit', $params);
    }

    function new (Request $request) {
        $params['model_info']['column_names'] = User::descrive();
        $params['model_info']['name'] = 'users';
        $params['model_info']['name_kana'] = 'アカウント';
        $params['column_infos'] = config('admin_db_info.column_infos')['users'];
        $params['model_info']['hide_columns'] = ['password', 'remember_token', 'id', 'created_at', 'updated_at'];
        $params['column_infos']['role']['input_values'] = [
            'owner' => 'オーナー',
            'staff' => 'スタッフ',
        ];

        return view('admin/users/new', $params);
    }

    public function update(Request $request, $id)
    {
        $validation = config('admin_db_info.validation')['users'];
        if ($request->has('password') && $request->input('password') === null) {
            unset($validation['password']);
        }
        $validation['email'] = [
            'required',
            'email',
            Rule::unique('users')->ignore($id),
        ];
        $this->validate($request, $validation);

        $user = User::where('id', $id)->firstOrFail();
        $params['record'] = $user;
        $params['model_info']['name'] = 'users';
        $params['model_info']['name_kana'] = 'アカウント';
        $params['column_infos'] = User::descrive();

        if ($request->has('password') && $request->input('password') !== null) {
            $user->password = Hash::make($request->input('password'));
            unset($validation['password']);
        }

        foreach ($validation as $column_name => $val) {
            if ($request->has($column_name)) {
                $user->$column_name = $request->input($column_name);
            }
        }
        if ($user->save()) {
            $request->session()->flash('flash_message', 'アカウントを更新しました');
        }
        return redirect('admin/users/edit/' . $user->id);
    }

    public function create(Request $request)
    {
        $validation = config('admin_db_info.validation')['users'];
        $validation['email'] = [
            'required',
            'email',
            Rule::unique('users'),
        ];

        $this->validate($request, $validation);

        $user = new User;

        $params['record'] = $user;
        $params['model_info']['name'] = 'users';
        $params['model_info']['name_kana'] = 'アカウント';
        $params['column_infos'] = User::descrive();
        $user->password = Hash::make($request->input('password'));
        unset($validation['password']);

        foreach ($validation as $column_name => $val) {
            if ($request->has($column_name)) {
                $user->$column_name = $request->input($column_name);
            }
        }

        if ($user->save()) {
            $request->session()->flash('flash_message', 'アカウントを新規作成しました');
        }

        return redirect('admin/users/edit/' . $user->id);
    }

    public function delete(Request $request, $id)
    {
        $this->validate($request, [
            'id' => 'required|integer',
        ]);
        $user = User::where('id', $id)->firstOrFail();
        $user->delete();
        $request->session()->flash('flash_message', 'アカウントを削除しました');
        return redirect('admin');
    }
}
