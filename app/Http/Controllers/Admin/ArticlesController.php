<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Cast;
use App\Category;
use App\Http\Controllers\Controller;
use App\Picture;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;


class ArticlesController extends Controller
{

    public function index(Request $request)
    {
        $params = [];
        if (Gate::allows('staff-higher')) {
            $articles = Article::orderBy('created_at', 'desc')->paginate(10);
            $params['create_record_buttons'] = [];
        } else {
            $user = Auth::user();
            $cast = $user->cast;
            $articles = Article::where('cast_id', $cast->id)->orderBy('created_at', 'desc')->paginate(10);
            $params['create_record_buttons'] = [
                ['name_kana' => 'ブログ記事', 'name' => 'articles', 'relation_record_id' => Auth::user()->cast->id],
            ];
            $params['cast'] = $cast;
        }

        if (Gate::allows('owner-only')) {
            $params['status_index'] = ['enable' => '公開', 'disable' => '非公開', 'ban' => '凍結'];
        } else {
            $params['status_index'] = ['enable' => '公開', 'disable' => '非公開', 'ban' => '非公開'];
        }

        $params['records'] = $articles;
        $params['column_infos'] = config('admin_db_info.column_infos')['articles'];
        $params['model_info']['name'] = 'articles';
        $params['model_info']['name_kana'] = 'ブログ';
        $params['model_info']['column_names'] = Article::descrive();
        $params['model_info']['hide_columns'] = ['app_name', 'body'];

        return view('admin/articles/index', $params);
    }

    public function edit(Request $request, $id)
    {

        $record = Article::where('id', $id)->firstOrFail();

        Log::debug($record->cast);

        if (!isset($record->cast) || !isset($record->cast->user_id) || Gate::denies('operation-articles', $record->cast->user_id)) {
            abort(404);
        }

        $params['record'] = $record;
        $params['column_infos'] = config('admin_db_info.column_infos')['articles'];
        $params['model_info']['name'] = 'articles';
        $params['model_info']['name_kana'] = '記事';
        $params['model_info']['column_names'] = Article::descrive();
        $params['model_info']['hide_columns'] = ['id', 'category_id', 'cast_id'];
        $params['model_info']['unalterable_columns'] = ['created_at', 'updated_at'];
        $params['model_info']['hide_columns'][] = 'app_name';

        if (Gate::denies('staff-higher')) {
            $params['model_info']['hide_columns'][] = 'status';
            $params['model_info']['hide_columns'][] = 'cast_id';

            $params['cast'] = Auth::user()->cast;
        } else {
            if (Gate::denies('owner-only')) {
                $params['column_infos']['status']['input_values'] = ['enable' => '公開', 'disable' => '非公開'];
            }
        }

        $params['categories'] = Category::all();
        $params['picture_info']['main'] = $params['record']->picture;
        $params['is_create_record_url'] = true;
        $params['is_delete_record_form'] = true;

        return view('admin/articles/edit', $params);
    }

    function new (Request $request, $id) {
        $params = [];
        $params['cast'] = Cast::where('id', $id)->firstOrFail();

        $params['model_info']['name'] = 'articles';
        $params['column_infos'] = config('admin_db_info.column_infos')['articles'];
        $params['model_info']['name_kana'] = '記事';
        $params['model_info']['column_names'] = Article::descrive();
        $params['model_info']['hide_columns'] = ['category_id', 'id', 'created_at', 'updated_at'];
        $params['model_info']['hide_columns'][] = 'cast_id';
        $params['model_info']['hide_columns'][] = 'app_name';

        if (Gate::denies('staff-higher')) {
            $params['model_info']['hide_columns'][] = 'status';

        }
        $params['categories'] = Category::all();
        $params['relation_record_id'] = $id;

        return view('admin/articles/new', $params);
    }

    public function delete(Request $request, $id)
    {
        $article = Article::where('id', $id)->firstOrFail();
        if (Gate::denies('operation-articles', $article->cast->user_id)) {
            abort(404);
        }
        $this->validate($request, [
            'id' => 'required|integer',
        ]);

        $article->delete();
        $request->session()->flash('flash_message', '記事を削除しました');
        return redirect('admin/articles/index');
    }

    public function update(Request $request, $id)
    {

        $article = Article::where('id', $id)->firstOrFail();

        if (Gate::denies('operation-articles', $article->cast->user_id)) {
            abort(404);
        }

        $validation = config('admin_db_info.validation')['articles'];
        unset($validation['app_name']);

        if (Gate::denies('staff-higher')) {
            unset($validation['status']);
            unset($validation['cast_id']);
        }
        $this->validate($request, $validation);
        unset($validation['picture']);
        unset($validation['category']);

        $params['record'] = $article;
        $params['model_info']['name'] = 'articles';
        $params['model_info']['name_kana'] = '記事';
        $params['column_infos'] = Article::descrive();
        $category_id = $request->input('category');
        $article->category_id = $category_id;

        foreach ($validation as $column_name => $val) {
            if ($request->has($column_name)) {
                $article->$column_name = $request->input($column_name);
            }
        }

        if (Gate::denies('staff-higher')) {
            $user = Auth::user();
            $article->cast_id = Cast::where('user_id', $user->id)->firstOrFail()->id;
        } else {
            $cast = Cast::where('id', $article->cast_id)->firstOrFail();
            $user = User::where('id', $cast->user_id)->firstOrFail();
        }

        if ($article->save()) {
            $picture_info = $request->file('picture');
            Picture::upload($picture_info, 'article', $article, 'main', $user->app_name);
            $request->session()->flash('flash_message', '更新しました');
        }

        return redirect('admin/articles/edit/' . $article->id);
    }

    public function create(Request $request, $id)
    {

        $validation = config('admin_db_info.validation')['articles'];
        unset($validation['cast_id']);
        unset($validation['app_name']);

        if (Gate::denies('staff-higher')) {
            unset($validation['status']);
        }
        $this->validate($request, $validation);
        $cast = Cast::where('id', $id)->firstOrFail();

        $article = new Article;
        unset($validation['picture']);
        unset($validation['category']);

        $params['record'] = $article;
        $params['model_info']['name'] = 'articles';
        $params['model_info']['name_kana'] = '記事';
        $params['column_infos'] = Article::descrive();
        $category_id = $request->input('category');
        $category = Category::where('id', $category_id)->firstOrFail();
        $article->category_id = $category->id;

        foreach ($validation as $column_name => $val) {
            if ($request->has($column_name)) {
                $article->$column_name = $request->input($column_name);
            }
        }
        if (Gate::denies('staff-higher')) {
            $user = Auth::user();
            $article->cast_id = Cast::where('user_id', $user->id)->firstOrFail()->id;
            $article->app_name = $user->app_name;
            $article->status = 'enable';
        } else {
            $cast_id = $cast->id;
            $article->cast_id = $cast_id;
            $cast = Cast::where('id', $cast_id)->firstOrFail();
            $user = User::where('id', $cast->user_id)->firstOrFail();
            $article->app_name = $user->app_name;
        }

        if ($article->save()) {
            $picture_info = $request->file('picture');
            Picture::upload($picture_info, 'article', $article, 'main', $user->app_name);
            $request->session()->flash('flash_message', '新規作成しました');
        }

        return redirect('admin/articles/index');
    }
}
