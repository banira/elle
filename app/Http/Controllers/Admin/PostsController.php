<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Picture;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class PostsController extends Controller
{

    public function index(Request $request)
    {
        $params = [];
        $posts = Post::orderBy('created_at', 'desc')->paginate(10);
        $params['records'] = $posts;
        $params['column_infos'] = config('admin_db_info.column_infos')['posts'];
        $params['model_info']['name'] = 'posts';
        $params['model_info']['name_kana'] = 'お知らせ';
        $params['model_info']['column_names'] = Post::descrive();
        $params['model_info']['hide_columns'] = ['body', 'updated_at'];
        $params['is_create_record_url'] = true;
        if (Gate::allows('owner-only')) {
            $params['status_index'] = ['enable' => '公開', 'disable' => '非公開', 'ban' => '凍結'];
        } else {
            $params['status_index'] = ['enable' => '公開', 'disable' => '非公開', 'ban' => '非公開'];
        }
        return view('admin/posts/index', $params);
    }

    public function edit(Request $request, $id)
    {
        $params['record'] = Post::where('id', $id)->firstOrFail();
        $params['column_infos'] = config('admin_db_info.column_infos')['posts'];
        $params['model_info']['name'] = 'posts';
        $params['model_info']['name_kana'] = 'お知らせ';
        $params['model_info']['column_names'] = Post::descrive();
        $params['model_info']['hide_columns'] = ['id', 'user_id', ''];
        $params['model_info']['unalterable_columns'] = ['id', 'created_at', 'updated_at', 'user_id'];
        $params['is_delete_record_form'] = true;
        $params['is_create_record_url'] = true;
        if (Gate::denies('owner-only')) {
            $params['column_infos']['status']['input_values'] = ['enable' => '公開', 'disable' => '非公開'];
        }

        $params['picture_info']['main'] = $params['record']->picture;

        $params['categories'] = Category::all();

        return view('admin/posts/edit', $params);
    }

    function new (Request $request) {
        $params['model_info']['name'] = 'posts';
        $params['column_infos'] = config('admin_db_info.column_infos')['posts'];
        $params['model_info']['name_kana'] = 'お知らせ';
        $params['model_info']['column_names'] = Post::descrive();
        $params['model_info']['hide_columns'] = ['id', 'created_at', 'updated_at', 'user_id'];
        return view('admin/posts/new', $params);
    }

    public function delete(Request $request, $id)
    {
        $this->validate($request, [
            'id' => 'required|integer',
        ]);
        $post = Post::where('id', $id)->firstOrFail();
        $post->delete();
        $request->session()->flash('flash_message', 'お知らせ情報を削除しました');
        return redirect('admin/posts/index');
    }

    public function update(Request $request, $id)
    {

        $validation = config('admin_db_info.validation')['posts'];
        $this->validate($request, $validation);
        unset($validation['picture']);

        $post = Post::where('id', $id)->firstOrFail();

        $params['record'] = $post;
        $params['model_info']['name'] = 'posts';
        $params['model_info']['name_kana'] = 'お知らせ';
        $params['column_infos'] = Post::descrive();

        foreach ($validation as $column_name => $val) {
            if ($request->has($column_name)) {
                $post->$column_name = $request->input($column_name);
            }
        }

        if ($post->save()) {
            $picture_info = $request->file('picture');
            Picture::upload($picture_info, 'post', $post, 'main', $post->app_name);
            $request->session()->flash('flash_message', 'お知らせを更新しました');
        }

        return redirect('admin/posts/edit/' . $post->id);
    }

    public function create(Request $request)
    {
        $validation = config('admin_db_info.validation')['posts'];
        $this->validate($request, $validation);
        unset($validation['picture']);

        $post = new Post;

        $params['record'] = $post;
        $params['model_info']['name'] = 'posts';
        $params['model_info']['name_kana'] = 'お知らせ';
        $params['column_infos'] = Post::descrive();

        foreach ($validation as $column_name => $val) {
            if ($request->has($column_name)) {
                $post->$column_name = $request->input($column_name);
            }
        }

        if ($post->save()) {
            $picture_info = $request->file('picture');
            Picture::upload($picture_info, 'article', $post, 'main', $post->app_name);
            $request->session()->flash('flash_message', 'お知らせを新規作成しました');
        }
        return redirect('admin/posts/index');
    }
}
