<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interview;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class InterviewsController extends Controller
{

    public function index(Request $request)
    {
        $params = [];
        $interviews = Interview::orderBy('created_at', 'desc')->paginate(10);
        $params['column_infos'] = config('admin_db_info.column_infos')['interviews'];
        $params['records'] = $interviews;
        $params['model_info']['name'] = 'interviews';
        $params['model_info']['name_kana'] = '質問';
        $params['model_info']['column_names'] = Interview::descrive();
        $params['model_info']['hide_columns'] = ['password', 'created_at', 'updated_at'];
        $params['is_create_record_url'] = true;
        $params['create_record_buttons'] = [
            ['name_kana' => '新しい質問', 'name' => 'interviews'],
        ];

        return view('admin/template/index', $params);
    }

    public function edit(Request $request, $id)
    {
        $params['record'] = Interview::where('id', $id)->firstOrFail();
        $params['column_infos'] = config('admin_db_info.column_infos.interviews');
        $params['model_info']['name'] = 'interviews';
        $params['model_info']['name_kana'] = '質問';
        $params['model_info']['column_names'] = Interview::descrive();
        $params['model_info']['hide_columns'] = ['id', 'remember_token', 'created_at', 'updated_at'];
        $params['model_info']['unalterable_columns'] = [];
        $params['is_delete_record_form'] = true;
        $params['is_create_record_url'] = true;

        return view('admin/template/edit', $params);
    }

    function new (Request $request) {
        $params['column_infos'] = config('admin_db_info.column_infos.interviews');
        $params['model_info']['name'] = 'interviews';
        $params['model_info']['name_kana'] = '質問';
        $params['model_info']['column_names'] = Interview::descrive();
        $params['model_info']['hide_columns'] = ['id', 'created_at', 'updated_at'];
        return view('admin/template/new', $params);
    }

    public function delete(Request $request, $id)
    {
        $this->validate($request, [
            'id' => 'required|integer',
        ]);
        $interview = Interview::where('id', $id)->firstOrFail();
        $interview->delete();
        $request->session()->flash('flash_message', 'キャストへの質問を削除しました');
        return redirect('admin/interviews/index');
    }

    public function update(Request $request, $id)
    {
        $validation = config('admin_db_info.validation.interviews');
        unset($validation['question']);
        $validation['question'] = [
            'required',
            Rule::unique('interviews')->ignore($id),
        ];
        $this->validate($request, $validation);
        unset($validation['password']);
        unset($validation['picture']);

        $interview = Interview::where('id', $id)->firstOrFail();

        $params['record'] = $interview;
        $params['model_info']['name'] = 'interviews';
        $params['model_info']['name_kana'] = 'キャストへの質問';
        $params['column_infos'] = Interview::descrive();

        foreach ($validation as $column_name => $val) {
            if ($column_name !== 'picture' && $request->has($column_name)) {
                $interview->$column_name = $request->input($column_name);
            }
        }

        if ($interview->save()) {

            $request->session()->flash('flash_message', 'キャストへの質問を更新しました');
        }

        return redirect('admin/interviews/edit/' . $interview->id);
    }

    public function create(Request $request)
    {
        $validation = config('admin_db_info.validation.interviews');
        $this->validate($request, $validation);

        $interview = new Interview;

        $params['record'] = $interview;
        $params['model_info']['name'] = 'interviews';
        $params['model_info']['name_kana'] = 'キャストへの質問';
        $params['column_infos'] = Interview::descrive();
        foreach ($validation as $column_name => $val) {
            if ($request->has($column_name)) {
                $interview->$column_name = $request->input($column_name);
            }
        }

        if ($interview->save()) {
            $request->session()->flash('flash_message', 'キャストへの質問を新規作成しました');
        }

        return redirect('admin/interviews/edit/' . $interview->id);
    }
}
