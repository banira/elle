<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
 /*
 |--------------------------------------------------------------------------
 | Login Controller
 |--------------------------------------------------------------------------
 |
 | This controller handles authenticating users for the application and
 | redirecting them to your home screen. The controller uses a trait
 | to conveniently provide its functionality to your applications.
 |
  */

 use AuthenticatesUsers;

 /**
  * Where to redirect users after login.
  *
  * @var string
  */
 protected $redirectTo = '/admin';

 /**
  * Create a new controller instance.
  *
  * @return void
  */
 public function __construct()
 {
  $this->middleware('guest')->except('logout');
 }

 //管理者ログインページ
 public function loginform(Request $request)
 {
  return view('guest.auth.login');
 }

 //管理者ログインチェックとリダイレクト
 public function login(Request $request)
 {

  $email = $request->input('email');
  $password = $request->input('password');
  if (Auth::attempt(['email' => $email, 'password' => $password])) {
   $request->session()->flash('flash_message', 'ログインに成功しました。');
   $user = Auth::user();
   if ($user->role === 'owner' || $user->role === 'staff') {
    return redirect('/admin');
   } else {
    return redirect('/admin/casts/edit/' . $user->cast->id);
   }

   return redirect()->intended('/admin');

  }
  return redirect('/');
 }

 //管理者ログアウト
 public function logout(Request $request)
 {
  Auth::logout();
  $request->session()->flash('flash_message', 'ログアウトに成功しました。');
  return redirect('/');
 }
}
