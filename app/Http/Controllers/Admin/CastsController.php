<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Cast;
use App\Http\Controllers\Controller;
use App\Interview;
use App\Picture;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class CastsController extends Controller
{
    public function update_attendance(Request $request, $id, $value)
    {
        $cast = Cast::where('id', $id)->firstOrFail();

        $cast->is_attendance_in = $value;
        $params['model_info']['name'] = 'casts';
        $params['model_info']['name_kana'] = 'キャスト情報';
        $params['column_infos'] = Cast::descrive();

        if ($cast->save()) {

            $request->session()->flash('flash_message', '申込を更新しました');
        }

        return redirect('admin/casts/index/');
    }

    public function index(Request $request)
    {
        if (Gate::allows('staff-higher')) {
            $params = [];
            $casts = Cast::orderBy('created_at', 'desc')->paginate(10);
            $params['column_infos'] = config('admin_db_info.column_infos')['casts'];
            $params['records'] = $casts;
            $params['model_info']['name'] = 'casts';
            $params['model_info']['name_kana'] = 'キャスト情報';
            $params['model_info']['column_names'] = Cast::descrive();

            $params['model_info']['hide_columns'] = ['password', 'remember_token'];
            $params['is_create_record_url'] = true;
            if (Gate::allows('owner-only')) {
                $params['status_index'] = ['enable' => '公開', 'disable' => '非公開', 'ban' => '凍結'];
            } else {
                $params['status_index'] = ['enable' => '公開', 'disable' => '非公開', 'ban' => '非公開'];
            }

            return view('admin/casts/index', $params);
        } else {

            $params = [];
            $user = Auth::user();
            $cast = $user->cast;

            $params['cast'] = $cast;
            $articles = Article::where('cast_id', $cast->id)->orderBy('created_at', 'desc')->paginate(10);

            $params['records'] = $articles;
            $params['column_infos'] = config('admin_db_info.column_infos')['articles'];
            $params['model_info']['name'] = 'articles';
            $params['model_info']['name_kana'] = '記事';
            $params['model_info']['column_names'] = Article::descrive();
            $params['model_info']['hide_columns'] = [];
            $params['is_create_record_url'] = true;
            $params['status_index'] = ['enable' => '公開', 'disable' => '非公開', 'ban' => '非公開'];

            return view('admin/articles/index', $params);
        }
    }

    public function edit(Request $request, $id)
    {
        $cast = Cast::where('id', $id)->firstOrFail();
        $params['record'] = $cast;
        if (Gate::denies('operation-casts', $params['record']->user_id)) {
            abort(404);
        }

        $params['column_infos'] = config('admin_db_info.column_infos.casts');
        $params['model_info']['name'] = 'casts';
        $params['model_info']['name_kana'] = 'キャスト情報';
        $params['model_info']['column_names'] = Cast::descrive();
        $params['model_info']['hide_columns'] = [
            'remember_token',
            'id',
            'user_id',
            'updated_at',
            'daily_access_count',
            'weekly_access_count',
            'monthly_access_count',
            'total_access_count',
            'created_at'];
        $params['model_info']['unalterable_columns'] = [
            'id',
            'created_at',
            'updated_at',
            'user_id',

        ];
        $params['create_record_buttons'] = [
            ['name_kana' => 'ブログ記事', 'name' => 'articles', 'relation_record_id' => $id],
        ];

        if (Gate::allows('staff-higher')) {
            $params['is_delete_record_form'] = true;
            $params['is_create_record_url'] = true;
            $params['create_record_buttons'][] = ['name_kana' => 'キャスト', 'name' => 'casts'];

            if (Gate::denies('owner-only')) {
                $params['column_infos']['status']['input_values'] = ['enable' => '公開', 'disable' => '非公開'];
            }
        } else {
            $params['model_info']['hide_columns'] = [
                'created_at',
                'updated_at',
                'id',
                'user_id',
                'daily_access_count',
                'weekly_access_count',
                'monthly_access_count',
                'total_access_count'];
            $params['column_infos']['status']['input_values'] = [
                'enable' => '公開',
                'disable' => '非公開',
            ];
            $params['cast'] = $cast;

        }
        $related_interviews = $params['record']->interviews->pluck('id')->all();
        $params['interviews'] = Interview::whereNotIn('id', $related_interviews)->get();
        $params['picture_info']['profile'] = Picture::where('cast_id', $id)->where('type', 'profile')->first();
        $params['picture_info']['header'] = Picture::where('cast_id', $id)->where('type', 'header')->first();

        return view('admin/casts/edit', $params);
    }

    function new (Request $request) {
        $params['column_infos'] = config('admin_db_info.column_infos')['casts'];
        $params['model_info']['name'] = 'casts';
        $params['model_info']['name_kana'] = 'キャスト情報';
        $params['model_info']['column_names'] = Cast::descrive();
        $params['model_info']['hide_columns'] = ['age', 'id', 'password', 'remember_token', 'created_at', 'updated_at', 'user_id', 'message', 'status', 'daily_access_count', 'weekly_access_count', 'monthly_access_count', 'total_access_count', 'is_attendance_in'];
        $params['model_info']['column_names'][] = 'email';
        $params['model_info']['column_names'][] = 'password';
        $params['model_info']['column_names'][] = 'app_name';

        $params['interviews'] = Interview::all();

        return view('admin/casts/new', $params);
    }

    public function delete(Request $request, $id)
    {
        $this->validate($request, [
            'id' => 'required|integer',
        ]);
        $cast = Cast::where('id', $id)->firstOrFail();
        $cast->delete();
        $request->session()->flash('flash_message', 'キャスト情報を削除しました');
        return redirect('admin/casts/index');
    }

    public function update(Request $request, $id)
    {
        $cast = Cast::where('id', $id)->firstOrFail();
        if (Gate::denies('operation-casts', $cast->user_id)) {
            abort(404);
        }
        $interviews = [];
        $validation = config('admin_db_info.validation')['casts'];

        if (Gate::denies('staff-higher')) {
            unset($validation['app_name']);
        } else {
            $validation['monthly_access_count'] = [
                'required',
            ];
        }
        if ($request->has('password') && $request->input('password') === null) {
            unset($validation['password']);
        }
        $validation['email'] = [
            'required',
            'email',
            Rule::unique('users')->ignore($cast->user_id),
        ];

        $this->validate($request, $validation);
        unset($validation['password']);
        unset($validation['email']);
        unset($validation['picture_profile']);
        unset($validation['picture_header']);
        unset($validation['status']);
        unset($validation['app_name']);

        $cast = Cast::where('id', $id)->firstOrFail();

        $user = User::where('id', $cast->user_id)->firstOrFail();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->status = $request->input('status');
        if (Gate::allows('staff-higher')) {
            $user->app_name = $request->input('app_name');
        }
        $user->role = 'cast';

        $params['record'] = $cast;
        $params['model_info']['name'] = 'casts';
        $params['model_info']['name_kana'] = 'キャスト情報';
        $params['column_infos'] = Cast::descrive();

        if ($request->has('password') && $request->input('password') !== null) {
            $user->password = Hash::make($request->input('password'));
            unset($validation['password']);
        }

        foreach ($validation as $column_name => $val) {
            if ($request->has($column_name)) {
                $cast->$column_name = $request->input($column_name);
            }
        }
        if (is_null($request->input('message')) || $request->input('message') === '') {
            $cast->message = '';
        }
        if (is_null($request->input('age')) || $request->input('age') === '') {
            $cast->age = '';
        }

        if ($request->has('interviews')) {
            foreach ($request->input('interviews') as $key => $value) {
                if (!is_null($value)) {
                    $interviews[(integer) $key] = ['answer' => $value];
                }
            }
        }

        if ($user->save()) {
            $cast->save();
            $cast->interviews()->sync($interviews);
            $picture_profile_info = $request->file('picture_profile');
            Picture::upload($picture_profile_info, 'cast', $cast, 'profile', $user->app_name);
            $picture_header_info = $request->file('picture_header');
            Picture::upload($picture_header_info, 'cast', $cast, 'header', $user->app_name);
            $request->session()->flash('flash_message', '申込を更新しました');
        }

        return redirect('admin/casts/edit/' . $cast->id);
    }

    public function create(Request $request)
    {
        $interviews = [];
        $validation = config('admin_db_info.validation')['casts'];
        $validation['email'] = [
            'required',
            'email',
            Rule::unique('users'),
        ];
        unset($validation['age']);
        unset($validation['picture_profile']);
        unset($validation['picture_header']);
        unset($validation['categories']);
        unset($validation['interviews']);
        unset($validation['status']);

        $this->validate($request, $validation);
        unset($validation['password']);
        unset($validation['email']);
        unset($validation['app_name']);
        unset($validation['picture_profile']);
        unset($validation['picture_header']);

        $user = new User;

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->status = 'disable';
        $user->role = 'cast';
        $user->app_name = $request->input('app_name');

        $params['model_info']['name'] = 'casts';
        $params['model_info']['name_kana'] = 'キャスト情報';
        $params['column_infos'] = Cast::descrive();
        $cast = new Cast;
        foreach ($validation as $column_name => $val) {
            if ($request->has($column_name)) {
                $cast->$column_name = $request->input($column_name);
            }
        }

        if ($user->save()) {
            $cast->user_id = $user->id;
            if ($cast->save()) {
                $request->session()->flash('flash_message', 'キャスト情報を新規作成しました');
            }

        }

        return redirect('admin/casts/index');
    }
}
