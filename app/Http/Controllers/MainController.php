<?php

namespace App\Http\Controllers;

use App\Article;
use App\Cast;
use App\Post;
use Illuminate\Http\Request;

class MainController extends Controller
{
    /**
     * トップページ
     * @return \Illuminate\Http\Response
     * @param \Illuminate\Http\Request $request
     */
    public function top(Request $request){
        $app_name = config('const.app_name');
        $articles = Article::where('status', 'enable')
            ->where('app_name', $app_name)
            ->whereHas('cast.user', function ($query) {$query->where('status', 'enable');})
            ->orderBy('updated_at', 'desc')
            ->take(5)
            ->get();
        $posts = Post::where('status', 'enable')
            ->where('app_name', $app_name)
            ->orderBy('updated_at', 'desc')
            ->take(3)
            ->get();
        $casts = Cast::whereHas('user', function ($query) use ($app_name) {
            $query->where('app_name', $app_name)->where('status', 'enable');
        });
        $ranking_casts = $casts;
        $ranking_casts = $ranking_casts->orderBy('monthly_access_count', 'desc')
            ->take(6)->get();

        $casts = $casts->orderBy('updated_at', 'desc')
            ->take(5)->get();

        return view('guest.main.top')
            ->with('app_info', config('const.' . $app_name))
            ->with('app_name', $app_name)
            ->with('articles', $articles)
            ->with('posts', $posts)
            ->with('ranking_casts', $ranking_casts)
            ->with('casts', $casts);
    }

    /**
     * 店舗情報
     * @return \Illuminate\Http\Response
     */
    public function shop()
    {
        $app_name = config('const.app_name');
        return view('guest.main.shop')
            ->with('app_name', $app_name)
            ->with('app_info', config('const.' . $app_name));
    }

    /**
     * プライバシー
     * @return \Illuminate\Http\Response
     */
    public function privacy()
    {
        $app_name = config('const.app_name');
        return view('guest.main.privacy')
            ->with('app_name', $app_name)
            ->with('app_info', config('const.' . $app_name));
    }

    /**
     * 採用情報
     * @return \Illuminate\Http\Response
     */
    public function recruit()
    {
        $app_name = config('const.app_name');
        return view('guest.main.recruit.' . $app_name)
            ->with('app_name', $app_name)
            ->with('app_info', config('const.' . $app_name));
    }

    /**
     * system
     * @return \Illuminate\Http\Response
     */
    public function system()
    {
        $app_name = config('const.app_name');
        return view('guest.main.system.' . $app_name)
            ->with('app_name', $app_name)
            ->with('app_info', config('const.' . $app_name));
    }
}
