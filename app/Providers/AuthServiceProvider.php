<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // ownerユーザーのみ許可
        Gate::define('owner-only', function ($user) {
            return ($user->role === 'owner');
        });
        // staffユーザー以上（ownerユーザー＆staffユーザー）に許可
        Gate::define('staff-higher', function ($user) {
            return ($user->role === 'owner' || $user->role === 'staff');
        });
        // castユーザー以上（つまり全権限）に許可
        Gate::define('cast-higher', function ($user) {
            return ($user->role === 'owner' || $user->role === 'staff' || $user->role === 'cast');
        });
        // castユーザーのみに許可
        Gate::define('cast-only', function ($user) {
            return ($user->role === 'cast');
        });

        Gate::define('operation-casts', function ($user, $user_id) {
            return $user->role === 'owner' || $user->role === 'staff' || (string) $user_id === (string) $user->id;
        });

        Gate::define('operation-articles', function ($user, $user_id) {
            return $user->role === 'owner' || $user->role === 'staff' || (string) $user_id === (string) $user->id;
        });
    }
}
