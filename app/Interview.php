<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Interview extends Model
{
    protected $guarded = ['id', '_token'];

    public static function descrive()
    {
        return collect(DB::select('DESCRIBE interviews'))->pluck('Field');
    }

    public function shop()
    {
        return $this->belongsTo('App\Shop');
    }

    public function casts()
    {
        return $this->belongsToMany('App\Cast')->withPivot('answer');
    }
}
