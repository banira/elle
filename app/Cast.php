<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Cast extends Model
{
    protected $guarded = ['id', '_token'];

    public static function descrive()
    {
        return collect(DB::select('DESCRIBE casts'))->pluck('Field');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function interviews()
    {
        return $this->belongsToMany('App\Interview')->withPivot('answer');
    }

    public function pictures()
    {
        return $this->hasMany('App\Picture');
    }

    public function articles()
    {
        return $this->hasMany('App\Article');
    }

    protected static function boot()
    {
        parent::boot();
        self::deleting(function ($cast) {
            $cast->articles()->delete();
        });
    }
}
