<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Post extends Model
{
    protected $guarded = ['id', '_token'];

    protected $dates = ['created_at', 'updated_at'];

    public static function descrive()
    {
        return collect(DB::select('DESCRIBE posts'))->pluck('Field');
    }

    public function shop()
    {
        return $this->belongsTo('App\Shop');
    }

    public function picture()
    {
        return $this->hasOne('App\Picture');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
