<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class Picture extends Model
{
    protected $guarded = ['id', '_token'];

    public function article()
    {
        return $this->belongsTo('App\Article');
    }

    public function cast()
    {
        return $this->belongsTo('App\Cast');
    }

    public function post()
    {
        return $this->belongsTo('App\Post');
    }

    //保存処理
    public static function custom_save($save_name, $model_name, $model, $type, $app_name)
    {

        $picture_obj = new Picture;
        $picture_obj->file_name = $save_name;
        $relation_key_column_name = $model_name . '_id';
        $picture_obj->$relation_key_column_name = $model->id;
        $picture_obj->type = $type;
        $picture_obj->app_name = $app_name;
        $picture_obj->model_name = $model_name;

        $picture_obj->save();
    }

    //画像を透かし,リサイズ加工の上リサイズ加工の上上書き保存
    public static function edit_picture($absolute_save_path, $resize_width = 0)
    {
        $image = Image::make($absolute_save_path);
        $width = $image->width();
        if ($width <= 500) {
            $resize_width = $width;
        }
        $image->resize($resize_width, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        $image->save();
    }

    public static function upload($picture_info, $model_name, $model, $type, $app_name)
    {
        return DB::transaction(function () use ($picture_info, $model_name, $model, $type, $app_name) {
            if (!is_null($picture_info) && $picture_info->isValid()) {
                switch (true) {
                    case $type === 'main' && $model_name === 'article':
                        $processing_infos = [
                            500 => 'original',
                            200 => 'thumbnail',
                        ];
                        break;
                    case $type === 'profile' && $model_name === 'cast':
                        $processing_infos = [
                            500 => 'original',
                            200 => 'thumbnail',
                        ];
                        break;
                    case $type === 'header' && $model_name === 'cast':
                        $processing_infos = [
                            500 => 'original',
                        ];
                    case $type === 'main' && $model_name === 'post':
                        $processing_infos = [
                            500 => 'original',
                            200 => 'thumbnail',
                        ];
                        break;
                };
                $ext = pathinfo($picture_info->getClientOriginalName(), PATHINFO_EXTENSION);
                $save_name = uniqid(rand(1000, 9999)) . '.' . $ext;
                //todo: 重複確認処理
                Picture::custom_save($save_name, $model_name, $model, $type, $app_name);
                foreach ($processing_infos as $width => $processing_name) {
                    $save_dir_path = $processing_name . '/';
                    $picture_info->storeAs(
                        $save_dir_path, $save_name, $app_name
                    );
                    Picture::edit_picture(config('filesystems.disks.' . $app_name)['root'] . '/' . $save_dir_path . $save_name, $width);
                }
            }
            // クエリ処理
            return true;
        });
    }

    public function getAbsoluteUrlPathAttribute()
    {
        return '/storage/' . $this->dir_path_name . $this->file_name;
    }

    public function scopeTypeFilter($query, $model_name, $id, $type_name, $processing_type_name)
    {
        $query = $query
            ->where($model_name . '_id', $id)
            ->where('type', $type_name)
            ->where('processing_type', $processing_type_name);
        return $query;
    }
}
