<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    public static function descrive()
    {
        return collect(DB::select('DESCRIBE categories'))->pluck('Field');
    }

    public function articles()
    {
        return $this->hasMany('App\Article');
    }

    protected $fillable = [
        'body',
    ];
}
