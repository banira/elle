<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\HtmlString;

class Article extends Model
{
    public static function descrive()
    {
        return collect(DB::select('DESCRIBE articles'))->pluck('Field');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function cast()
    {
        return $this->belongsTo('App\Cast');
    }

    public function picture()
    {
        return $this->hasOne('App\Picture');
    }

    //viewでbodyカラム出力時のラッパーメゾット
    public function getArticleBody()
    {
        return new HtmlString($this->body);
    }

    protected $fillable = [
        'body',
        'created_at',
    ];

    protected $dates = ['created_at', 'updated_at'];

}
